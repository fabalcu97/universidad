#ifndef RSA_H
#define RSA_H


#include <iostream>
#include <NTL/ZZ.h>
#include <vector>
#include <string>
#include <limits>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <algorithm>

using namespace std;
using namespace NTL;


class RSA
{
    public:

        RSA(int);                   ///Generador claves
        RSA(ZZ, ZZ);

        string cifrado(string, ZZ, ZZ);
        string descifrado(string, ZZ, ZZ);
        void generar(int);
        int contar_cifras(int);
        void restaurar(string);
        string recibirFirma(string);
        string enviarFirma();
        string RestoChino(string);
        string CompletarBloque(string, int);
        ZZ InversaFermat();

    private:
        ZZ  other_e,
            other_n,
            n,
            e,
            d,
            p,
            q,
            dq,
            dp,
            phi_n;

        string abc;
        int last_pos;


};

#endif // RSA_H
