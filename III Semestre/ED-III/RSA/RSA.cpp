#include "RSA.h"

#include "/home/fabricio/Programming/Discretas III/libmath.h"
#include "/home/fabricio/Programming/Discretas III/random.h"
#include "/home/fabricio/Programming/Discretas III/random_primos.h"

RSA::RSA(int bits){                                             ///Emisor

    do{
        p = Aleatorio_primos((bits>>1)-2);
    }while(!Fermat(p));

//    cout<<endl<<"Genero P"<<endl;
    do{
        q = Aleatorio_primos((bits>>1)-2);
    }while(!Fermat(q) || q == p);

    n = (p)*(q);
    phi_n = (p-1)*(q-1);

    do{

        e = Aleatorio(bits>>1);

    }while( Euclides(e, phi_n) != 1 || e < 2);

    d = Euclides_ext(e, phi_n);

    if ( d < 0 ){
        d += phi_n;
    }

//    cout<<endl<<"                     Su P es : "<<p<<endl<<endl;
//    cout<<"                     Su Q es : "<<q<<endl<<endl;
//    cout<<"                     Phi_N: "<<phi_n<<endl<<endl;

    cout<<endl<<"                     Su E es : "<<e<<endl<<endl;
    cout<<"                     Su D es : "<<d<<endl<<endl;
    cout<<"                     Su N es : "<<n<<endl<<endl;
    abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 1234567890,;:-_<>@*+().\n";
    last_pos = contar_cifras(abc[abc.size()-1]);

    string archivo = "ClavesNuevas";
    stringstream b;
    b << bits;
    string bitss = b.str();
    archivo += bitss;

    archivo += ".txt";

    ofstream file(archivo);
    file<<p<<endl;
    file<<q<<endl;
    file<<e<<endl;
    file<<d<<endl;
    file<<n<<endl;
    file.close();


}

RSA::RSA(ZZ priv, ZZ mod){          ///Receptor

    conv(d, priv);
    conv(n, mod);

}

string RSA::cifrado(string m, ZZ e, ZZ mo){

//    cout<<endl<<"Mensaje: "<<m<<endl<<endl;

    this->other_e = e;

    this->other_n = mo;

    string mensaje, car, mensaje_cifrado, bin_E, temp;
    ZZ cif, aux1;

    int tam_a = m.size(), bloque;

    for (int i = 0; i < tam_a; i++){

        car = to_string( abc.find(m[i]) );

        mensaje += CompletarBloque(car, last_pos);

    }

    stringstream a;

    a << other_n;
    temp = a.str();
    bloque = temp.size();

    while( mod(mensaje.size(), bloque-1) != 0 ){
        mensaje += "23";
    }

//    cout<<"Mensaje: "<<mensaje<<endl;

    tam_a = mensaje.size();
    a.str("");
    bin_E = binarios_ZZ(other_e);

    for (int i = 0; i < tam_a; i += bloque-1){

        temp = mensaje.substr(i, bloque-1);
        stringstream b(temp);
        b >> aux1;

        cif = fast_exp(aux1, bin_E, other_n );
//        cout<<endl<<"A Cifrar: "<<mensaje.substr(i, bloque-1)<<endl<<endl;
//        cout<<"Cifrado : "<<cif<<endl;

        a << cif;
        car = a.str();
        a.str("");

//        cout<<endl<<"Cifrado : "<<CompletarBloque(car, bloque)<<endl<<endl;

        mensaje_cifrado += CompletarBloque(car, bloque);

    }
    mensaje_cifrado += "#";
    mensaje_cifrado += enviarFirma();

    return mensaje_cifrado;
}

string RSA::descifrado(string m, ZZ e, ZZ mo){

//    cout<<"Mensaje: "<<m<<endl;

    this->other_e = e;
    this->other_n = mo;

    string mensaje, car, mensaje_descifrado, bin_D, temp, firma;

    ZZ descif, aux1;

    int tam_a = m.size(), bloque, aux, i;

    stringstream a;

    a << n;
    car = a.str();
    bloque = car.size();

    bin_D = binarios_ZZ(d);

    a.str("");

    for (i = 0; m[i] != '#'; i += bloque){

        temp = m.substr(i, bloque);

        car = RestoChino(temp);

        mensaje += CompletarBloque(car, (bloque-1));

    }
    i++;

    tam_a = mensaje.size();

//    cout<<"Mensaje: "<<mensaje<<endl;

    for (int j = 0; j < tam_a; j += 2){

        temp = mensaje.substr(j, 2);
        stringstream b(temp);
        b >> aux;

        mensaje_descifrado += abc[aux];
    }

    cout<<endl<<"Terminá descifrado, inicia Firma"<<endl;

    cout<<"Mensaje: "<<mensaje_descifrado<<endl;

    firma = recibirFirma(m.substr(i));

    mensaje_descifrado += "#";
    mensaje_descifrado += firma;



    return mensaje_descifrado;

}

int RSA::contar_cifras(int car){

    stringstream a;
    string temp;

    a << car;
    temp = a.str();

    return temp.size();

}

string RSA::enviarFirma(){

    string  temp1 = "C",
            res,
            temp,
            car,
            firma;

    int bloque;

    ZZ cif, cif1, cif2, aux1;

    stringstream n;
    n << abc.find(temp1);
    res = n.str();

//        cout<<"e: "<<e<<endl;
//        cout<<"d: "<<d<<endl;
//        cout<<"n: "<<n<<endl;
//        cout<<"other_e: "<<other_e<<endl;
//        cout<<"other_n: "<<other_n<<endl;

    stringstream a;
    a << other_n;
    temp = a.str();
    bloque = temp.size();

    res = RestoChino(res);

//    cout<<"Res1: "<<res<<endl;

    while( mod(res.size(), bloque-1) != 0 ){
        res.insert(res.begin(), '0');
    }
    cout<<"Res2: "<<res<<endl;
    stringstream c(res);
    c >> cif;


    if (cif > other_n){

        stringstream a;

        for (int i = (res.length()-bloque+1); i >= 0; i -= bloque-1){

//            cout<<endl<<"A Cifrar: "<<res.substr(i, bloque-1)<<endl<<endl;

            temp = res.substr(i, bloque-1);
            stringstream b(temp);
            b >> aux1;

            cif2 = fast_exp(aux1, binarios_ZZ(other_e), other_n);

            a << cif2;
            car = a.str();
            a.str("");

            firma = CompletarBloque(car, bloque) + firma;

//            cout<<"Bloque final: "<<CompletarBloque(car, bloque)<<endl;

        }
//        cout<<"Firma final: "<<firma<<endl;

        return firma;
    }

//    cout<<endl<<endl<<"Rúbrica: "<<cif<<endl;
    else{
        cif1 = fast_exp(cif, binarios_ZZ(other_e), other_n);
    }
//    cout<<endl<<endl<<"Firma: "<<firma<<endl;

    stringstream b;
    b << cif1;
    res = b.str();

    return CompletarBloque(res, (bloque));
}

string RSA::recibirFirma(string firma){

    ZZ cif, cif1;
    int r, bloque;
    string res, temp, temp1;

//    cout<<endl<<endl<<"Firma: "<<firma<<endl;

    stringstream a;
    a << n;
    temp = a.str();
    bloque = temp.size();

    for (int i = 0; i < firma.length(); i += bloque){

//        cout<<"bloque: "<<firma.substr(i, bloque)<<endl;

        temp1 = firma.substr(i, bloque);
//        cout<<"Rúbrica: "<<temp1<<endl;
        temp = RestoChino(temp1);
        temp = CompletarBloque(temp, bloque-1);
//        cout<<"RES: "<<temp<<endl;
        res += temp;


    }

//    cout<<"RES: "<<res<<endl;

    stringstream b(res);
    b >> cif;

//    cout<<"CIF: "<<cif<<endl;

    cif1 = fast_exp(cif, binarios_ZZ(other_e), other_n);

//    cout<<endl<<endl<<"Rúbrica: "<<cif1<<endl;

    conv(r, cif1);
//    cout<<"E: "<<other_e<<endl;
//    cout<<"N: "<<other_n<<endl;

//    cout<<endl<<"R: "<<r<<endl;

    res = abc[r];
//    cout<<"Res: "<<res<<endl;
    return res;
}

string RSA::RestoChino(string mensaje){

    ZZ a, b, q_1, q_2, des, aux;

    string bin_dp, bin_dq, descifrado, bin_p, bin_q;

    dp = mod_ZZ(d, p-1);
    dq = mod_ZZ(d, q-1);

    bin_dp = binarios_ZZ(dp);
    bin_dq = binarios_ZZ(dq);
    bin_q = binarios_ZZ(q-2);
    bin_p = binarios_ZZ(p-2);

    stringstream cast(mensaje);
    cast >> des;

    a = fast_exp(des, bin_dp, p);
    b = fast_exp(des, bin_dq, q);

    q_1 = fast_exp(q,bin_p, p);

    if (q_1 < 0){
        q_1 += p;
    }

    q_2 = fast_exp(p,bin_q, q);

    if (q_2 < 0){
        q_2 += q;
    }

    des = ((a)*(q)*(q_1)) + ((b)*(p)*(q_2));

//    cout<<"Des: "<<des<<endl;

    des = mod_ZZ(des, n);

//    cout<<"Des: "<<des<<endl;

    stringstream cas;
    cas << des;
    descifrado += cas.str();

//    cout<<"Descifrado: "<<descifrado<<endl;

    return descifrado;
}

string RSA::CompletarBloque(string completar, int tam_bloque){

    int h;
    h = completar.size();

    if ( h  < tam_bloque){
        completar.insert(completar.begin(),tam_bloque-h, '0');
        return completar;
    }

    return completar;

}

void RSA::restaurar(string nombre_archivo){

    ifstream file(nombre_archivo);
    string temp;
    int i = 0;

    while( i < 5 ){

        getline(file, temp);
        stringstream b(temp);

        if ( i == 0){
            b >> this->p;
        }
        if ( i == 1){
            b >> this->q;
        }
        if ( i == 2){
            b >> this->e;
        }
        if ( i == 3){
            b >> this->d;
        }
        if ( i == 4){
            b >> this->n;
        }
        i++;
    }

}








