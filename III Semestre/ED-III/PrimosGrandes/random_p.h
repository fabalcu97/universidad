#ifndef RANDOM_P_H_INCLUDED
#define RANDOM_P_H_INCLUDED

#include <iostream>
#include <string>
#include <time.h>
#include <random>
#include <algorithm>
#include <NTL/ZZ.h>

#include "/home/fabricio/Programming/Discretas III/libmath.h"

using namespace std;
using namespace NTL;



string binarios(int x){

    string temp;
    if (x == 0){
        temp.push_back(0);
        temp.push_back(1);
        return temp;
    }
    while (x != 0)
    {
        temp.insert(temp.begin(), ('0' + (x & 1)));
        x = x >> 1;
    }

    return temp;
}

ZZ conversion(string binario){


    ZZ rd;
    rd = 0;
    long long  lo, l = binario.size();

    for (int i = l-1; i >= 0; i--){

        if (binario[i] == '1'){
            rd += power_ZZ(2, lo);

        }

//        if (mod(i,16) == 0){
//            cout<<"i: "<<i<<endl;
//        }

        lo++;
    }
    return rd;

}

string corrimiento_der(string temp){

    int vueltas,tap;
    vueltas=mod(rand(),temp.size());

    for (; vueltas > 0; vueltas--){
        tap = mod(rand(), temp.size());
        temp.insert(temp.begin(), '0' + ((*temp.end() + temp[tap])&1));
        temp.pop_back();
    }
//    cout<<temp;
    return temp;
}

string corrimiento_izq(string temp){

    int vueltas,tap;
    vueltas = mod(rand(),temp.size());

    for (; vueltas > 0; vueltas--){
        tap = mod(rand(), temp.size());
        temp.push_back('0' + ((temp[0] + temp[tap])&1));
        temp.erase(temp.begin());
    }
//    cout<<temp;
    return temp;

}


string corrimiento(string aleatorio){

    int i = 0,
        part = 2 ,
        tam = aleatorio.size(),
        espacio;

    string  temp,
            resultado;

    if ( tam > 64 ){
        part = tam >> 5;
    }
    espacio = (tam / part);
    while( i < tam){

        if (part == 1){
            temp = aleatorio.substr(i);
        }
        else{
            temp = aleatorio.substr(i, espacio);
        }

        if (part&1){
            temp = corrimiento_der(temp);
        }
        else{
            temp = corrimiento_izq(temp);
        }
        i += espacio;
        resultado += temp;
    }

    /*for(; i < tam; i += espacio
    ){

       if(i == tam-1){

           resultado += aleatorio[i];
       }
       else{

           temp = aleatorio.substr(i, espacio);

           if(aleatorio[i]&1){
               resultado += corrimiento_der(temp);
           }
           else{
               resultado += corrimiento_izq(temp);
           }
       }
    }*/

    resultado[resultado.size() - 1] = '1';

    return resultado;
}


ZZ random(int _tamanio){

    //Inicializando variables

    string semilla;

    int _vueltas, seed, tap, _long;

    //asignacion de variables

    seed = mod(rand(), 100);

    _vueltas  = mod(rand(), _tamanio);

    semilla = binarios(seed);

    _long = semilla.size();

    //extendiendo la semilla

    for (int i = 0; i < (_tamanio - _long); i++){
        semilla.push_back('0' + ( (semilla[i+1] + semilla[i]) & 1));
    }

    _long = semilla.size();

    //corrimiento de la semilla

    for(int i=0; i < _vueltas; i++){

       tap= mod(rand(), _tamanio);

       semilla.push_back('0' + ((semilla[0] + semilla[tap])&1));
       semilla.erase(semilla.begin());
    }

    //convirtiendo a decimal la semilla
    semilla = corrimiento(semilla);
    return conversion(semilla);
}





#endif // RANDOM_P_H_INCLUDED
