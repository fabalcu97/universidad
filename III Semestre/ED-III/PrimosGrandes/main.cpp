
#include </home/fabricio/Programming/Discretas III/Trabajo_Parcial/random_p.h>
#include </home/fabricio/Programming/Discretas III/Trabajo_Parcial/Test_Primos.h>

using namespace std;

int main(){

    srand(time(NULL));

    Test_Primos x;
    ZZ iteration ,cero, num,base;
    iteration = 50;
    cero=0;
    base=2;
    int j=0, c, b;
    cout<<"----------------Test Miller Rabin----------------"<<endl<<endl;
    cout<<"Ingrese la cantidad de primos que desea generar: ";
    cin>>c;
    cout<<endl<<"Ingrese la cantidad de bits que desea generar: ";
    cin>>b;
    cout<<endl;
    clock_t tStart = clock();

    for(int k=0;k<c;k++)
    {
        do
        {

            num = random(b);


        }while((x.Miller(num, iteration))==false);

        cout<<num<<endl<<endl<<"-------------------------ES PRIMO-------------------------"<<endl<<endl;
    }

    cout<<"TIEMPO-------->"<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<endl;

    return 0;
}
