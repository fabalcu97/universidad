#ifndef TEST_PRIMOS_H
#define TEST_PRIMOS_H
#include <NTL/ZZ.h>

using namespace NTL;

class Test_Primos
{
    public:

        Test_Primos();
        ZZ get_modulo(ZZ,ZZ);
        ZZ mulmod(ZZ a, ZZ b, ZZ mod);
        ZZ modulo(ZZ,ZZ,ZZ);
        bool Miller(ZZ p,ZZ iteration);

    private:
};

#endif // TEST_PRIMOS_H
