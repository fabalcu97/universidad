#include <iostream>
#include "Test_Primos.h"
#include <NTL/ZZ.h>
#include <stdlib.h>
#include <time.h>


using namespace NTL;
using namespace std;


Test_Primos::Test_Primos()
{
    //ctor
}


ZZ Test_Primos::get_modulo(ZZ a,ZZ b)///Modulo ZZ
{
    if(a>0)
        return a-(b*(a/b));
    return a-(b*((a/b)-1));
}


///Exponenciacion Modular
ZZ Test_Primos::modulo(ZZ base, ZZ exponent, ZZ mod)
{
    ZZ b,x,y;
    x=1;
    b=2;
    y=base;

    while (exponent > 0)
    {
        if (get_modulo(exponent,b) == 1)
            x = get_modulo((x * y),mod);
        y = get_modulo((y * y),mod);
        exponent = exponent >> 1;
    }
    return get_modulo(x,mod);
}


ZZ Test_Primos::mulmod(ZZ a, ZZ b, ZZ mod)
{
    ZZ base,x, y ;
    x=0;
    base=2;
    y=get_modulo(a,mod);
    while (b > 0)
    {
        if (get_modulo(b,base) == 1)
        {
            x = get_modulo((x + y),mod);
        }
        y = get_modulo((y << 1),mod);
        b = b>>1;
    }
    return get_modulo(x,mod);
}


bool Test_Primos::Miller(ZZ p,ZZ iteration)
{
    ZZ base;
    base=2;
    if (p < 2)
    {
        return false;///Elimina el 1 ya que no es primo
    }
    if (p != 2 && (get_modulo(p,base)==0))///
    {
        return false;///Termina en numeros pares
    }
    ZZ s;
    s = p - 1;
    while (get_modulo(s,base) == 0)
    {
        s = s >> 1;
    }
    long i;

    for (i = 0; i < iteration; i++)
    {
        ZZ a , mod , temp;
        SetSeed(ZZ(INIT_VAL, time(NULL)));
        long v;
        v=2048;
        a = get_modulo(RandomBits_ZZ(v),(p - 1))+ 1;
//        cout<<" aleatoria para evaluar: "<<a<<endl;
        temp = s;
        mod = modulo(a, temp, p);
        while (temp != p - 1 && mod != 1 && mod != p - 1)
        {
            mod = mulmod(mod, mod, p);
            temp = temp << 1;
        }
        if (mod != p - 1 && temp % 2 == 0)
        {
            return false;
        }
    }
    return true;
}
