#!/usr/local/bin/perl
 use WordNet::QueryData;
 use WordNet::Similarity::path;
  my $wn = WordNet::QueryData->new;
  my $measure = WordNet::Similarity::path->new ($wn);
  my $value = $measure->getRelatedness("car#n#1", "cat#n#2");
  my ($error, $errorString) = $measure->getError();
  die $errorString if $error;
  print "car (sense 1) <-> tripod (sense 2) = $value\n";
