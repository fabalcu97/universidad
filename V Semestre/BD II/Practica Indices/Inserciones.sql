﻿CREATE FUNCTION llenar_clientes()
RETURNS void AS
$BODY$BEGIN
for i in 1..1500 loop
	INSERT INTO cliente(nom_cliente) VALUES ( CONCAT('cliente_', i));
end loop;
END$BODY$
LANGUAGE "plpgsql" VOLATILE;

SELECT llenar_clientes();
SELECT * FROM cliente;
DROP FUNCTION llenar_clientes();

/*////////////////////////////////*/

CREATE FUNCTION llenar_categorias()
RETURNS void AS
$BODY$BEGIN
for i in 1..50loop
	INSERT INTO categoria(nom_categoria) VALUES ( CONCAT('categoria_', i));
end loop;
END$BODY$
LANGUAGE "plpgsql" VOLATILE;

SELECT llenar_categorias();
SELECT * FROM categoria;
DROP FUNCTION llenar_categorias()

/*////////////////////////////////*/

CREATE FUNCTION llenar_productos()
RETURNS void AS
$BODY$BEGIN
for i in 1..3000loop
	INSERT INTO producto(nom_producto, id_categoria) VALUES ( CONCAT('producto_', i), ( floor(random() * 49.5) + 1:: INTEGER));
end loop;
END$BODY$
LANGUAGE "plpgsql" VOLATILE;

SELECT llenar_productos();
SELECT * FROM producto;
DROP FUNCTION llenar_productos()

/*////////////////////////////////*/

CREATE FUNCTION llenar_ventas()
RETURNS void AS
$BODY$BEGIN
for i in 1..30000000loop
	INSERT INTO venta(id_producto, cantidad, fecha, id_cliente) VALUES (
		( floor(random() * 2999.5) + 1 :: INTEGER),
		( floor(random() * 499.5) + 1 :: INTEGER),
		((timestamp '2017-01-01 00:00:00' + random() * (timestamp '2016-01-01 00:00:00' - timestamp '2016-12-31 00:00:00') ) :: DATE ),
		( floor(random() * 1499.5) + 1:: INTEGER) );
end loop;
END$BODY$
LANGUAGE "plpgsql" VOLATILE;

SELECT llenar_ventas();
SELECT * FROM venta;
DROP FUNCTION llenar_ventas()
