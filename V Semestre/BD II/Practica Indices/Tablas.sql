DROP TABLE producto;
CREATE TABLE producto(
	id_producto SERIAL,
	nom_producto TEXT NOT NULL,
	id_categoria INTEGER NOT NULL
);

DROP TABLE categoria;
CREATE TABLE categoria(
	id_categoria SERIAL,
	nom_categoria TEXT NOT NULL
);

DROP TABLE cliente;
CREATE TABLE cliente(
	id_cliente SERIAL,
	nom_cliente TEXT NOT NULL
);

DROP TABLE venta;
CREATE TABLE venta(
	id_producto SERIAL,
	cantidad TEXT NOT NULL,
	fecha DATE NOT NULL,
	id_cliente INT NOT NULL
);
