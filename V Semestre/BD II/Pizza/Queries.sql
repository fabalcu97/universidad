/*A*/
Select pizzeria FROM Frequents
INNER JOIN Person
ON Frequents.name=Person.name and Person.age<18;

/*B*/
Select pizzeria FROM Frequents
INNER JOIN Person
ON Frequents.name=Person.name and Person.age<18;

/*C*/
Select Person.name FROM Person
INNER JOIN Eats
ON Eats.name=Person.name and (Eats.pizza='mushroom' or Eats.pizza='pepperoni') and Person.gender='female'
GROUP BY (Person.name)
HAVING COUNT(Person.name)=2;

/*D*/
Select Frequents.pizzeria FROM Frequents
INNER JOIN Eats
ON Frequents.name='Amy' and Frequents.name=Eats.name
INNER JOIN Serves
ON Eats.pizza=Serves.pizza and Frequents.pizzeria=Serves.pizzeria and Serves.price>=10.00;

/*E*/
SELECT frequents.pizzeria
FROM person
INNER JOIN Frequents
ON person.name=frequents.name
AND person.gender='female';

/*F*/


/*G*/


/*H*/


/*I*/
SELECT pizzeria, MIN(serves.price) AS PRICE
FROM serves
WHERE serves.pizza='pepperoni'
GROUP BY pizzeria
ORDER BY price ASC LIMIT 2
