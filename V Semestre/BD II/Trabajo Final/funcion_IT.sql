--DROP FUNCTION similarity_IT1(TEXT);

CREATE OR REPLACE FUNCTION similarity_IT(text)
RETURNS TABLE(sim FLOAT, pal TEXT) AS
$BODY$
DECLARE
	i BIGINT;
	j BIGINT;
	k BIGINT;
	tmp corpus_final%rowtype;
	tmpp1 palabra1%rowtype;
	tmp1 INT;
	id INT;
	tmp2 FLOAT;
	tmp3 FLOAT;
	tmp4 BIGINT;
	tmp5 BIGINT;
	tmp6 FLOAT;
	tmpr FLOAT;
	tmp7 TEXT;
	result FLOAT;
	pal1_w BIGINT[];
	pal1_r FLOAT[];
	pal2_r FLOAT[];

BEGIN
	i := 1;
	j := 1;
	k := 1;
	tmp1 := 0;		--Cantidad de repeticion de palabra1
	tmp2 := 0;
	tmp3 := 0;
	tmp6 := 0;
	result := 0;

	SELECT rep INTO tmp1 FROM palabra1 WHERE palabra = $1;
	SELECT id_palabra INTO id FROM palabra1 WHERE palabra = $1;

	IF tmp1 IS NULL THEN
		RETURN;
	END IF;

	pal1_w := array_fill(0, ARRAY[tmp1]);
	pal1_r := array_fill(0, ARRAY[tmp1]);
	pal2_r := array_fill(0, ARRAY[tmp1]);

	FOR tmp IN SELECT * FROM corpus_final WHERE palabra1 = id LOOP
		pal1_w[j] = tmp.palabra2;
		pal1_r[j] = tmp.rank;
		j := j + 1;
	END LOOP;

	SELECT COUNT(palabra) INTO tmp5 FROM palabra1;
	tmp5 := tmp5 + 1;

	WHILE ( i < tmp5 ) LOOP

		SELECT id_palabra, palabra INTO tmp4, tmp7 FROM palabra1 WHERE id_palabra = i;

		FOR j IN 1..tmp1 LOOP

			SELECT rank INTO tmpr FROM corpus_final WHERE palabra1 = tmp4 AND palabra2 = pal1_w[j];

			pal2_r[j] = tmpr;
			IF ( tmpr IS NULL ) THEN
				pal2_r[j] = 0;
			END IF;

			tmp2 := tmp2 + pal1_r[j] * pal2_r[j];
			tmp3 := tmp3 + (pal1_r[j]^2);
			tmp6 := tmp6 + (pal2_r[j]^2);
		END LOOP;


		result := 0;
		tmp3 := sqrt(tmp3);
		tmp6 := sqrt(tmp6);
		IF ( tmp3 != 0 AND tmp6 != 0) THEN
			result = tmp2/(tmp3*tmp6);
		END IF;

		pal := tmp7;
		sim := result;
		RAISE NOTICE 'Palabra: %', i;

		RETURN NEXT;
		IF (tmp4 >= 10) THEN
			RETURN;
		END IF;

		pal2_r := array_fill(0, ARRAY[tmp1]);
		i := i + 1;
		tmp2 := 0;
		tmp3 := 0;
		tmp6 := 0;

	END LOOP;

	RETURN;
END
$BODY$
LANGUAGE 'plpgsql' ;
