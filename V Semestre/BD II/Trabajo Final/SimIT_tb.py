#!/usr/bin/python
from time import time
import math
import psycopg2
import psycopg2.extras
from decimal import Decimal

try:
    conn = psycopg2.connect("dbname='TrabajoFinal' user='postgres' host='localhost' port='5433' password='root'")
    conn.autocommit = True
except:
    print("I am unable to connect to the database")

cur = conn.cursor()
cur.execute("DROP table resultados")
cur.execute("CREATE TABLE resultados(palabra text,sim double precision)")

i = 1
j = 1
k = 1
tmp1 = 0
tmp2 = 0
tmp3 = 0
tmp6 = 0
aux=0
id=0
pal1_w=[]
pal1_r=[]
pal2_r=[]
result=[]

palabra=raw_input()
tiempo_inicial = time()
cur.execute("SELECT COUNT(palabra) FROM palabra2")
aux=cur.fetchall()[0][0]

for i in range(aux):
   pal1_r.append(0)

cur.execute("SELECT rep FROM palabra1 WHERE palabra ='"+palabra+"'");
tmp1 = cur.fetchall()[0][0]
cur.execute("SELECT id_palabra FROM palabra1 WHERE palabra ='"+palabra+"'");
id=cur.fetchall()[0][0]

cur.execute("SELECT * FROM corpus_final WHERE palabra1 = "+str(id)+" order by palabra1")
rows=cur.fetchall()
for row in rows:
   pal1_w.append(row[2])
   pal1_r[row[2]-1]=row[3]

cur.execute("SELECT COUNT(palabra) FROM palabra1")
tmp5=cur.fetchall()[0][0]

for i in range(tmp5):
   result.append(0)

tmp5=tmp5+1

i=1
tiempo_acum =0
cur.execute("SELECT palabra FROM palabra1")
tmpp1=cur.fetchall()
while i<tmp5:
   tiempo=time()
   cur.execute("SELECT palabra2,rank FROM corpus_final WHERE palabra1 = "+str(i))   
   tmp=cur.fetchall()
   
   tiempof=time()
   tiempo_acum = tiempo_acum+tiempof - tiempo
   for row in tmp:
      j=row[0]-1
      if pal1_r[j]!=0:
         tmp2 = tmp2 + pal1_r[j] * row[1]
         tmp3 = tmp3 + (pal1_r[j]**2)
         tmp6 = tmp6 + (row[1]**2)

   if ( tmp3 != 0 and tmp6 != 0):
      tmp3 = math.sqrt(tmp3)
      tmp6 = math.sqrt(tmp6)
      result[i-1] = tmp2/(tmp3*tmp6)
   pal=tmpp1[i-1][0]
   sim=result
   #print pal,sim,"\n"
   i=i+1
   tmp2 = 0
   tmp3 = 0
   tmp6 = 0
tiempo_final = time()
tiempo_ejecucion = tiempo_final - tiempo_inicial
print tiempo_ejecucion
print tiempo_acum

for i in range(tmp5-1):
   cur.execute("INSERT INTO resultados (palabra,sim) values(%s,"+str(result[i])+")",(str(tmpp1[i][0]),))

cur.execute("SELECT * FROM resultados order by sim desc limit 50")
rows=cur.fetchall()
for row in rows:
   print row[1],row[0]

#rows = cur.fetchall()
#n=0.0
#for row in rows:
#   n=0.0+row[0]
#   print "Palabra ", row[1]
#   print "Similaridad", n,"\n"

