
CREATE TABLE corpus1(

	id_rel SERIAL NOT NULL,
	palabra1 TEXT,
	palabra2 TEXT,
	rank BIGINT
);

CREATE INDEX idx11 ON corpus1 USING btree(palabra1);
CREATE INDEX idx12 ON corpus1 USING btree(palabra1, palabra2);

COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0000' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0001' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0002' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0003' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0004' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0005' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0006' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0007' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0008' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0009' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0010' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0011' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0012' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0013' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0014' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0015' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0016' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0017' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0018' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0019' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0020' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0021' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0022' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0023' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0024' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0025' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0026' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0027' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0028' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0029' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0030' (DELIMITER ' ');
COPY  corpus1(palabra1, palabra2, rank) FROM '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/2gm-0031' (DELIMITER ' ');

COPY (SELECT palabra1,palabra2,sum(rank) FROM corpus1 GROUP BY palabra1,palabra2) TO '/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusReducido' (DELIMITER ' '); 


