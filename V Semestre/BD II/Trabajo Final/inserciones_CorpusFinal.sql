--DROP FUNCTION LlenarCorpusFinal();
CREATE OR REPLACE FUNCTION LlenarCorpusFinal() RETURNS SETOF corpus AS
$BODY$
DECLARE

	j BIGINT;

	tmp palabra1%rowtype;
	tmp3 corpus%rowtype;
	tmp6 palabra2%rowtype;
	tmp4 INT;
	cnt BIGINT;

	pal_w TEXT[];
	pal_r FLOAT[];

BEGIN
	j := 1;

	SELECT COUNT(palabra) INTO cnt FROM palabra1;

	FOR tmp in (SELECT * FROM palabra1) LOOP
		j = 1;

		--Repeticiones de cada palabra1
		tmp4 := tmp.rep;
		pal_w := array_fill(0, ARRAY[tmp4]);
		pal_r := array_fill(0, ARRAY[tmp4]);

		FOR tmp3 IN (SELECT * FROM corpus WHERE palabra1 = tmp.palabra) LOOP
			pal_w[j] = tmp3.palabra2;
			pal_r[j] = tmp3.rank;

			SELECT * INTO tmp6 FROM palabra2 WHERE palabra = tmp3.palabra2;
			
			pal_r[j] = pal_r[j] / tmp.mrank;
			pal_r[j] = pal_r[j] * log(cnt/tmp6.doc_rep);

			INSERT INTO corpus_final(palabra1, palabra2, rank) VALUES(tmp.id_palabra, tmp6.id_palabra, pal_r[j]);

			j := j + 1;
		END LOOP;

	END LOOP;
	RETURN;
END
$BODY$
LANGUAGE 'plpgsql';

--SELECT * FROM LlenarCorpusFinal();
