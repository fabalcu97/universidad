--DROP FUNCTION LlenarPal1();
CREATE OR REPLACE FUNCTION LlenarPal1() RETURNS SETOF palabra1 AS
$BODY$
DECLARE
	i BIGINT;
	j BIGINT;
	flag BOOLEAN;
	tmp palabra1%rowtype;
	tmp1 TEXT;
	tmp2 BIGINT;
	tmp3 BIGINT;

BEGIN

	FOR tmp1 in (SELECT DISTINCT palabra1 FROM corpus) LOOP

		--SELECT COUNT(palabra1) INTO tmp2 FROM corpus WHERE palabra1=tmp1;
		--SELECT MAX(rank) INTO tmp3 FROM corpus WHERE palabra1 = tmp1;

		INSERT INTO palabra1(palabra, rep, mrank) SELECT palabra1, COUNT(palabra1), MAX(rank) FROM corpus WHERE palabra1=tmp1 GROUP BY palabra1;;

		--RETURN NEXT tmp1;
	END LOOP;
	RETURN;
END
$BODY$
LANGUAGE 'plpgsql';

--SELECT * FROM LlenarPal1();
