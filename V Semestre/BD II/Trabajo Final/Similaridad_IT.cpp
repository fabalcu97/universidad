#include <iostream>
#include <math.h>
#include <utility>
#include <vector>
#include <pqxx/pqxx>

using namespace std;
using namespace pqxx;

/*
	palabra1(id_palabra, rep, mrank, palabra)
	palabra2(id_palabra, doc_rep, palabra)
	corpus(id_rel, palabra1, palabra2, rank)
	corpus_final(id_rel, palabra1, palabra2, rank)
*/

int main(int, char *argv[])
{
	connection c("dbname=TrabajoFinal user=postgres");
	work txn(c);

	string pal2, stmp;

	int rep = 0,
		id_pal1 = 0,
		mrank = 0,
		cpal = 0,
		id_pal2 = 0;

	int* pal1_w;
	long double* pal1_r;
	long double* pal2_r;
	vector< pair<string, float> > Vrslt;

	float 	tmp1 = 0,
			tmp2 = 0,
			tmp3 = 0,
			rslt = 0;

	result r = txn.exec("SELECT * FROM palabra1 WHERE palabra = " + txn.quote(argv[1]) );

	if ( r.empty() ){
		cerr<<"No se econtró "<<argv[1]<<endl;
		return 1;
	}

	id_pal1 = r[0][0].as<int>();
	rep = r[0][1].as<int>();

	pal1_w = new int[rep];
	pal1_r = new long double[rep];
	pal2_r = new long double[rep];

	r = txn.exec("SELECT COUNT(palabra) FROM palabra1");
	cpal = r[0][0].as<int>();

	r = txn.exec("SELECT * FROM corpus_final WHERE palabra1 =" +to_string(id_pal1));
	for (int i = 0; i < rep; i++) {
		pal1_w[i] = r[i][2].as<int>();
		pal1_r[i] = r[i][3].as<long double>();
	}

	const clock_t begin_time = clock();

	for (int i = 0; i < 11; ++i) {

		r = txn.exec("SELECT * FROM palabra1 WHERE id_palabra = " + txn.quote(i));
		if ( r.empty() ){
			continue;
		}
		pal2 = r[0][3].as<string>();
		id_pal2 = r[0][0].as<int>();

		for (int j = 0; j < rep; ++j) {
			// cout<<id_pal2<<"-"<<pal1_w[j]<<endl;
			//
			//
			// for (int i = 0; i < r[0].size(); ++i) {
			// 	cout<<r[0][i]<<"-";
			// }
			// cout<<endl;
			// pal2_r[j] = 0;

			r = txn.exec("SELECT * FROM corpus_final WHERE palabra1 = "+to_string(id_pal2)+" AND palabra2 = " + txn.quote(pal1_w[j]));
			if ( !r.empty() ){
				pal2_r[j] = r[0][3].as<long double>();
			}

			tmp1 += pal1_r[j] * pal2_r[j];
			tmp2 += pow(pal1_r[j], 2);
			tmp3 += pow(pal2_r[j], 2);
			//cin>>stmp;
		}

		rslt = 0;
		tmp2 = sqrt(tmp2) * sqrt(tmp3);
		if (tmp2 != 0) {
			rslt = tmp1/tmp2;
		}

		pair<string, long double> tmp(pal2, rslt);

		Vrslt.push_back(tmp);
		tmp1 = tmp2 = tmp3 = 0;

		cout<<pal2<<endl;

	}

	std::cout << float( clock () - begin_time ) /  CLOCKS_PER_SEC;

	for (int i = 0; i < Vrslt.size(); i++) {
		cout<<"Palabra: "<<Vrslt[i].first<<"		|"<<"Similaridad: "<<Vrslt[i].second<<endl;
	}
	cin>>stmp;
}
