drop table corpus_vec;
CREATE TABLE corpus_vec
(
  id_vec integer NOT NULL,
  vec_id integer[],
  vec_rank double precision[]
  
);

CREATE INDEX idx15 ON corpus_vec USING HASH(id_vec);

CREATE OR REPLACE FUNCTION LlenarVec() RETURNS SETOF palabra1 AS
$BODY$
DECLARE
	tmp1 BIGINT;
	tmp2 INT;
	tmp corpus_final%rowtype;
	pal1_id BIGINT[];
	pal1_r FLOAT[];
	i BIGINT;
	j BIGINT;
	k BIGINT;
	

BEGIN

	SELECT COUNT(palabra) INTO tmp1 FROM palabra1;
	k := 1;
	FOR i IN 1..tmp1 LOOP
		j := 1;
		SELECT rep INTO tmp2 FROM palabra1 WHERE id_palabra = k;
		pal1_id := array_fill(0, ARRAY[tmp2]);
		pal1_r := array_fill(0, ARRAY[tmp2]);
		FOR tmp IN SELECT * FROM corpus_final WHERE palabra1 = k LOOP
			pal1_id[j] = tmp.palabra2;
			pal1_r[j] = tmp.rank;
			j := j + 1;
		END LOOP;
		INSERT INTO corpus_vec(id_vec,vec_id,vec_rank) VALUES(k, pal1_id, pal1_r);
		k := k + 1;

		--RETURN NEXT tmp1;
	END LOOP;
	RETURN;
END
$BODY$
LANGUAGE 'plpgsql';

select * from LlenarVec();
