--DROP FUNCTION similarity_RNK(TEXT);

CREATE OR REPLACE FUNCTION similarity_RNK(text) RETURNS TABLE(sim FLOAT, pal TEXT) AS
$BODY$
DECLARE
	i BIGINT;
	j BIGINT;
	k BIGINT;
	tmp corpus%rowtype;
	tmp1 INT;
	tmp2 FLOAT;
	tmp3 FLOAT;
	tmp4 TEXT;
	tmp5 INT;
	tmp6 FLOAT;
	pal1_w TEXT[];
	pal1_r BIGINT[];
	pal2_r BIGINT[];
	result FLOAT[];

BEGIN
	i := 1;
	j := 1;
	k := 1;
	tmp1 := 0;		--Cantidad de repeticion de palabra1
	tmp2 := 0;
	tmp3 := 0;
	tmp6 := 0;

	SELECT rep INTO tmp1 FROM palabra1 WHERE palabra = $1;
	SELECT COUNT(palabra) INTO tmp5 FROM palabra1;

	--tmp1 := tmp1 + 1;

	pal1_w := array_fill(0, ARRAY[tmp1]);
	pal1_r := array_fill(0, ARRAY[tmp1]);
	pal2_r := array_fill(0, ARRAY[tmp1]);
	result := array_fill(0, ARRAY[tmp5]);

	FOR tmp IN SELECT * FROM corpus WHERE palabra1 = $1 LOOP
		pal1_w[j] = tmp.palabra2;
		pal1_r[j] = tmp.rank;
		j := j + 1;
	END LOOP;

	tmp5 := tmp5 + 1;

	WHILE ( i < tmp5 ) LOOP
		j := 1;
		SELECT palabra INTO tmp4 FROM palabra1 WHERE id_palabra = i;

		WHILE ( j < tmp1 ) LOOP
			SELECT * INTO tmp FROM corpus WHERE palabra1 = tmp4 AND palabra2 = pal1_w[j];
			pal2_r[j] = tmp.rank;
			IF ( tmp IS NULL ) THEN
				pal2_r[j] = 0;
			END IF;
			j := j + 1;
		END LOOP;

		FOR k IN 1..tmp1 LOOP		--Hace los calculos para determinar la similitud entre palabra1 y palabra2
			tmp2 := tmp2 + pal1_r[k] * pal2_r[k];
			tmp3 := tmp3 + (pal1_r[k]^2);
			tmp6 := tmp6 + (pal2_r[k]^2);
		END LOOP;
		k := 1;

		result[i] := 0;
		tmp3 := sqrt(tmp3);
		tmp6 := sqrt(tmp6);
		IF ( tmp3 != 0 AND tmp6 != 0) THEN
			result[i] = tmp2/(tmp3*tmp6);
		END IF;

		pal := tmp4;
		sim := result[i];
		RAISE NOTICE 'Palabra: %', tmp4;
		RETURN NEXT;

		IF ( i > 10 ) THEN
			RETURN;
		END IF;

		pal2_r := array_fill(0, ARRAY[tmp1]);
		i := i + 1;
		tmp2 := 0;
		tmp3 := 0;
		tmp6 := 0;
	END LOOP;
	--RETURN sim;
	RETURN;
END
$BODY$
LANGUAGE 'plpgsql' ;
