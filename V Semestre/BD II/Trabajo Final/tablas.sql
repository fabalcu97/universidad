
DROP TABLE corpus;
CREATE TABLE corpus(

	id_rel SERIAL NOT NULL,
	palabra1 TEXT,
	palabra2 TEXT,
	rank BIGINT
);

CREATE INDEX idx1 ON corpus USING btree(palabra1);
CREATE INDEX idx2 ON corpus USING btree(palabra1, palabra2);

DROP TABLE corpus_final;
CREATE TABLE corpus_final(

	id_rel SERIAL NOT NULL,
	palabra1 INT,
	palabra2 INT,
	rank FLOAT
);

CREATE INDEX idx3 ON corpus_final USING btree(palabra1);
CREATE INDEX idx4 ON corpus_final USING btree(palabra1, palabra2);

DROP TABLE palabra1;
CREATE TABLE palabra1(
	id_palabra SERIAL NOT NULL,
	rep BIGINT,
	Mrank BIGINT,
	palabra TEXT
);

CREATE INDEX idx5 ON palabra1 USING hash(palabra);
CREATE INDEX idx6 ON palabra1 USING hash(id_palabra);

DROP TABLE palabra2;
CREATE TABLE palabra2(
	id_palabra SERIAL NOT NULL,
	doc_rep BIGINT,
	palabra TEXT
);

CREATE INDEX idx7 ON palabra2 USING hash(palabra);
CREATE INDEX idx8 ON palabra2 USING hash(id_palabra);
