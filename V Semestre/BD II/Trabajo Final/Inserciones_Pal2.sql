/*
--DROP FUNCTION LlenarPal2();
CREATE OR REPLACE FUNCTION LlenarPal2() RETURNS SETOF TEXT AS
$BODY$
DECLARE

	tmp1 TEXT;
	tmp2 BIGINT;

BEGIN

	FOR tmp1 in (SELECT DISTINCT palabra2 FROM corpus) LOOP

		SELECT COUNT(palabra2) INTO tmp2 FROM corpus WHERE palabra2 = tmp1;

		INSERT INTO palabra2(palabra, doc_rep) VALUES(tmp1, tmp2);

	END LOOP;
	RETURN;
END
$BODY$
LANGUAGE 'plpgsql';

SELECT * FROM LlenarPal2();
*/

INSERT INTO palabra2(doc_rep, palabra) SELECT COUNT(palabra1), palabra2 FROM corpus GROUP BY palabra2;
