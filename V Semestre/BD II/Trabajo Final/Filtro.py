import nltk
import re
import csv
import sys
from nltk.corpus import wordnet
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
reload(sys)
sys.setdefaultencoding('utf8')

if (wordnet.synsets(row[0])):
	print 'holi'
else:
	print 'noli'

files = ["2gm-0000", "2gm-0001", "2gm-0002", "2gm-0003", "2gm-0004", "2gm-0005", "2gm-0006",
		"2gm-0007", "2gm-0008", "2gm-0009", "2gm-0010", "2gm-0011", "2gm-0012", "2gm-0013",
		"2gm-0014", "2gm-0015", "2gm-0016", "2gm-0017", "2gm-0018", "2gm-0019", "2gm-0020",
		"2gm-0021", "2gm-0022", "2gm-0023", "2gm-0024", "2gm-0025", "2gm-0026", "2gm-0027",
		"2gm-0028", "2gm-0029", "2gm-0030", "2gm-0031"]

wnl=WordNetLemmatizer()
stops = set(stopwords.words('english'))
csv.field_size_limit(10000000000000)

for i in files:
	print i
	ref=open("/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/CorpusLematizado/"+i,"w")
	with open("/home/fabricio/Programming/V_Semestre/BD-II/Trabajo Final/Corpus/"+i) as f:
		c = csv.reader(f, delimiter='\t', skipinitialspace=True)

		for row in c:

			row[0] = row[0].lower()
			row[1] = row[1].lower()

			if ( wordnet.synsets(row[0]) and wordnet.synsets(row[1]) and (row[0] not in stops) and (row[1] not in stops) ):

				ref.write( row[0] )
				ref.write(" ")
				ref.write( wnl.lemmatize( row[1] ) )
				ref.write(" ")
				ref.write( row[2] )
				ref.write("\n")

	f.close
	ref.close
