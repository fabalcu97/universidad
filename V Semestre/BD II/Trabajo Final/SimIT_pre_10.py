#!/usr/bin/python
from Tkinter import *
from time import time
import math
import psycopg2
import psycopg2.extras
from decimal import Decimal
from PIL import Image, ImageTk

class MultiListbox(Frame):
    def __init__(self, master, lists):
	Frame.__init__(self, master)
	self.lists = []
	for l,w in lists:
	    frame = Frame(self); frame.pack(side=LEFT, expand=YES, fill=BOTH)
	    Label(frame, text=l, borderwidth=1, relief=RAISED).pack(fill=X)
	    lb = Listbox(frame, width=w, borderwidth=0, selectborderwidth=0,
			 relief=FLAT, exportselection=FALSE)
	    lb.pack(expand=YES, fill=BOTH)
	    self.lists.append(lb)
	    lb.bind('<B1-Motion>', lambda e, s=self: s._select(e.y))
	    lb.bind('<Button-1>', lambda e, s=self: s._select(e.y))
	    lb.bind('<Leave>', lambda e: 'break')
	    lb.bind('<B2-Motion>', lambda e, s=self: s._b2motion(e.x, e.y))
	    lb.bind('<Button-2>', lambda e, s=self: s._button2(e.x, e.y))
	frame = Frame(self); frame.pack(side=LEFT, fill=Y)
	Label(frame, borderwidth=1, relief=RAISED).pack(fill=X)
	sb = Scrollbar(frame, orient=VERTICAL, command=self._scroll)
	sb.pack(expand=YES, fill=Y)
	self.lists[0]['yscrollcommand']=sb.set

    def _select(self, y):
	row = self.lists[0].nearest(y)
	self.selection_clear(0, END)
	self.selection_set(row)
	return 'break'

    def _button2(self, x, y):
	for l in self.lists: l.scan_mark(x, y)
	return 'break'

    def _b2motion(self, x, y):
	for l in self.lists: l.scan_dragto(x, y)
	return 'break'

    def _scroll(self, *args):
	for l in self.lists:
	    apply(l.yview, args)

    def curselection(self):
	return self.lists[0].curselection()

    def delete(self, first, last=None):
	for l in self.lists:
	    l.delete(first, last)

    def get(self, first, last=None):
	result = []
	for l in self.lists:
	    result.append(l.get(first,last))
	if last: return apply(map, [None] + result)
	return result
	    
    def index(self, index):
	self.lists[0].index(index)

    def insert(self, index, *elements):
	for e in elements:
	    i = 0
	    for l in self.lists:
		l.insert(index, e[i])
		i = i + 1

    def size(self):
	return self.lists[0].size()

    def see(self, index):
	for l in self.lists:
	    l.see(index)

    def selection_anchor(self, index):
	for l in self.lists:
	    l.selection_anchor(index)

    def selection_clear(self, first, last=None):
	for l in self.lists:
	    l.selection_clear(first, last)

    def selection_includes(self, index):
	return self.lists[0].selection_includes(index)

    def selection_set(self, first, last=None):
	for l in self.lists:
	    l.selection_set(first, last)

def getKey(item):
   return item[0]


try:
    conn = psycopg2.connect("dbname='TrabajoFinal2' user='postgres' host='localhost' password='root' port='5433'")
    conn.autocommit = True
except:
    print("I am unable to connect to the database")

cur = conn.cursor()

master = Tk()
master.wm_title("Pre-procesamiento 10 vectores")

img = ImageTk.PhotoImage(Image.open('gugel.png'))
panel = Label(master, image = img)
panel.pack(side = "top", fill = "both", expand = "yes")

e = Entry(master)
e.pack()

e.focus_set()
def callback():
   
	mlb.delete(0,END)
	mlb.update_idletasks()
	i = 1
	j = 1
	k = 1
	tmp1 = 0
	tmp2 = []
	tmp3=0
	tmp6 = []
	aux=0
	id=0
	pal1_w=[]
	pal1_r=[]
	pal2_r=[]
	result=[]
	l=[]
	results=0

	palabra=e.get()
	tiempo_inicial = time()
	cur.execute("SELECT COUNT(palabra) FROM palabra2")
	aux=cur.fetchall()[0][0]

	for i in range(aux):
	   pal1_r.append(0)

	cur.execute("SELECT rep FROM palabra1 WHERE palabra ='"+palabra+"'");
	tmp1 = cur.fetchall()[0][0]
	cur.execute("SELECT id_palabra FROM palabra1 WHERE palabra ='"+palabra+"'");
	id=cur.fetchall()[0][0]

	cur.execute("SELECT * FROM corpus_vec WHERE id_vec = "+str(id))
	rows=cur.fetchall()
	for i in range(tmp1):
	   pal1_w.append(rows[0][1][i])
	   pal1_r[rows[0][1][i]-1]=rows[0][2][i]
	   tmp3 = tmp3 + (rows[0][2][i]**2)
	tmp3 = math.sqrt(tmp3)

	cur.execute("SELECT COUNT(palabra) FROM palabra1")
	tmp5=cur.fetchall()[0][0]
	for i in range(tmp5):
	   result.append(0)
	   tmp2.append(0)
	   tmp6.append(0)
	tmp5=tmp5+1

	i=1
	tiempo_acum =0
	cur.execute("SELECT palabra FROM palabra1")
	tmpp1=cur.fetchall()
	while i<tmp5:
	   tiempo=time()
	   cur.execute("SELECT * FROM corpus_vec WHERE id_vec = "+str(i)+" or id_vec = "+str(i+1)+" or id_vec = "+str(i+2)+" or id_vec = "+str(i+3)+" or id_vec = "+str(i+4)+" or id_vec = "+str(i+5)+" or id_vec = "+str(i+6)+" or id_vec = "+str(i+7)+" or id_vec = "+str(i+8)+" or id_vec = "+str(i+9))   
	   tmp=cur.fetchall()
	   
	   tiempof=time()
	   tiempo_acum = tiempo_acum+tiempof - tiempo
	   for row in tmp:
	      tmp1 = len(row[1])
	      for j in range(tmp1):
		 tmp2[row[0]-1] = tmp2[row[0]-1] + pal1_r[row[1][j]-1] * row[2][j]
		 tmp6[row[0]-1] = tmp6[row[0]-1] + (row[2][j]**2)
	   
	   for k in range(10):
	      if ( k+i-1<tmp5-1 and tmp6[k+i-1] != 0):
		 tmp6[k+i-1] = math.sqrt(tmp6[k+i-1])
		 result[k+i-1] = tmp2[k+i-1]/(tmp3*tmp6[k+i-1])
		 #print k+i,tmpp1[k+i-1][0],result[k+i-1]
	      if ( k+i-1<tmp5-1):
		 pal=tmpp1[k+i-1][0]
		 l.append([result[k+i-1],pal])
	   i=i+10

	tiempo_final = time()
	tiempo_ejecucion = tiempo_final - tiempo_inicial
	print tiempo_ejecucion
	print tiempo_acum         

	l=sorted(l,key=getKey,reverse=True)
	contador=0
	f = open('resultado', 'w');
	for i in l:
	    f.write( str.format("{0:f}", i[0]) +"   |   " + str(i[1]) + "\n")
	    contador=contador+1
	f.close()
	cont=0
	for item in l:
		if cont>100:
			break
		mlb.insert(END,(item[0],item[1]))
		cont=cont+1

mlb = MultiListbox(master, (('Similaridad', 40), ('Palabra', 20)))
mlb.pack(expand=YES,fill=BOTH)   
b = Button(master, text="Determinar Similitudes", width=20, command=callback)
b.pack()

mainloop()
e = Entry(master, width=50)
e.pack()

text = e.get()

#rows = cur.fetchall()
#n=0.0
#for row in rows:
#   n=0.0+row[0]
#   print "Palabra ", row[1]
#   print "Similaridad", n,"\n"

