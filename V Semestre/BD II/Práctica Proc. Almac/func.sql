CREATE OR REPLACE FUNCTION num(num INT) RETURNS TEXT AS $$
DECLARE str TEXT;
BEGIN
	IF ( $1 < 0 ) THEN
		str ="Negativo (-)";
	ELSEIF ( $1 >= 0 ) THEN
		 str = "Positivo (+)";
	END IF;
	RETURN str;
END
$$ LANGUAGE plpgsql;



///////////////////////////////

CREATE OR REPLACE PROCEDURE num(num integer) AS
BEGIN
	DELETE FROM numero WHERE numero.numero = 1;
END;
$BODY$
LANGUAGE plpgsql VOLATILE


//////////////////////////////


BEGIN;
	SAVEPOINT hhh;
	CREATE TABLE numero2(
		numero_id SERIAL NOT NULL,
		numero INTEGER NOT NULL,
		primary key(numero_id)
	);
	ROLLBACK TO hhh;
	COMMIT;
	--EXCEPTION WHEN OTHERS THEN
	--	ROLLBACK TO hhh;
Am
