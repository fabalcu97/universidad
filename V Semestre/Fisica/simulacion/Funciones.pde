
float x_axis = 0;
float y_axis = 0;
float x_0 = 1;
float y_0 = 0;

float speed = 50;   // m/s
float rotacion = 45;
float ang = 0;  // 85°
float G = 9.8;
float time = 0;
float t_tasa = 0.01;
float speed_y = 0;

volatile boolean flag = true;
volatile boolean end_flag = false;
volatile boolean ang_flag = false;
volatile boolean t_flag = false;
volatile boolean vel_flag = false;
volatile boolean lanz_flag = false;
float t = 20;
float x = 0;
float y = 0;

float speed_x = 0;
float Tv = 1;
float Tv_usuario = 1;
float max_x = 0;
float max_y = 0;

float x_vector[];
float y_vector[];
float t_vector[];
int lng = 0;
int it = 0;

float x() {
  if (cos(ang) < 0) {
    return 0;
  }
  return speed * cos(ang) * time;
}

float x_speed() {
  if (cos(ang) < 0) {
    return 0;
  }
  return speed * cos(ang);
}

float max_X(){
  return Tv * speed_x;
}

float y() {
  return y_0 + speed * sin(ang) * time - ( G * sq(time) / 2);
}

float y1(float pos_x) {
  return (tan(ang)) * pos_x - ( G * sq(pos_x) / ( 2 * sq((speed* (cos(ang)) ))));
}

float y_speed(float vel, float time) {
  return vel - G*time;
}

float t_vuelo() {
  return (2 * speed * sin(ang))/G;
}

float max_Y() {
  return (sq( speed * sin(ang) ))/(G);
}