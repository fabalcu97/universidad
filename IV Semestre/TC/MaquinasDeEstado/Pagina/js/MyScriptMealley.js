

var Fila = 0,
	Columna = 0,
	RadioFlag = 0,
    MatrixFlag = 0;
	init = 0,
	Estados = 0,
    Entradas = 0,
    Inputs = [],
    States = [],
    Outputs = [],
    Values = [],
    Tabla_Completa = [],
    Estado_Inicial;


function createCell(cell, text, text1, text2) {
    var div = document.createElement('input');

    div.setAttribute("size", "7");    
    div.setAttribute('id', text+"_"+text1+"_"+text2);
    cell.appendChild(div);
}

function createState(cell, text, style) {
    
    var div = document.createElement('input');

    div.setAttribute("size", "7");
    div.setAttribute('class', 'text');
    div.setAttribute('id', "Estado_"+text);
    cell.appendChild(div);
}

function RadioCheck(id) {

    var tmp;

    for (var i = 1; i < Fila && RadioFlag != 1; i++){
        tmp = document.getElementById('Radbtn'+i);
        tmp.checked = false;
    }
    id.checked = true;
}

function createIState(cell) {
    var div = document.createElement('input');
    div.setAttribute("type", "radio");
    div.setAttribute('id', "Radbtn" + Fila); 
    div.setAttribute("onclick", "RadioCheck(this)");
    cell.appendChild(div);
}

//Agregar Fila
function appendRow() {
    if (init != 0 && Fila < document.getElementById('NumEstados').value){
        var tbl1 = document.getElementById('Entradas'),
            tbl2 = document.getElementById('Salidas'),
            tbl3 = document.getElementById('Inicio'),
            row1 = tbl1.insertRow(tbl1.rows.length),
            row2 = tbl2.insertRow(tbl2.rows.length),
            row3 = tbl3.insertRow(tbl3.rows.length);

        createIState(row3.insertCell(0), "Salida");
        for (i = 0; i < tbl1.rows[0].cells.length; i++) {
            createCell(row1.insertCell(i), Fila, i, 'Entradas');
            createCell(row2.insertCell(i), Fila, i, 'Salidas');
        }
        Fila++;
        init = 1;
    }
    else{
        if (Fila >= document.getElementById('NumEstados').value){
            alert("No puede introducir más filas que estados")
        }
        else{
            alert("¡Ingrese el número de Estados porfavor!")
        }
    }
}

function appendEstados() {
    var Estados = document.getElementById('NumEstados').value;
    var tbl1 = document.getElementById('Estados');

    for (var i = 0; i < Estados; i++) {
        createState(tbl1.insertRow(i), i, 'Salida');
    }
}

function appendEntradas() {
    if (document.getElementById('NumEstados').value == "Número de Estados" || document.getElementById('NumEntradas').value == "Número de Entradas"){
        return;
    }
    if(init == 0){
        appendEstados();
        var Entradas = document.getElementById('NumEntradas').value;
        var tbl1 = document.getElementById('Entradas');
        var tbl2 = document.getElementById('Salidas');
        var row1 = tbl1.insertRow(tbl1.rows.length),
            row2 = tbl2.insertRow(tbl2.rows.length);

        for (var i = 0; i < Entradas; i++) {
            createCell(row1.insertCell(i), "Entrada", i, "Entrada");
            createCell(row2.insertCell(i), "Entrada", i, "Entrada");
        }
        init = 1;
    }
}

// delete table rows with index greater then 0
function deleteRows() {
    var tbl = document.getElementById('Mi_Tabla'),
        lastRow = tbl.rows.length - 1;

    if (lastRow != 0){
        tbl.deleteRow(lastRow);
    }
    Fila--;
}

function SetValue(id){
	/*var txt = document.getElementById('NumEstados');*/
	id.setAttribute('value', ' ');
}

function GenMatrix() {
    if (MatrixFlag == 0 /*&& fila != 0 && Columna != 0*/){
        var Sts = document.getElementById('Estados'),
            Ips = document.getElementById('Entradas'),
            Ops = document.getElementById('Salidas'),
            Entradas = parseInt(document.getElementById('NumEntradas').value),
            Estados = parseInt(document.getElementById('NumEstados').value),
            i,
            j,
            tmp;

        for ( i = 0; i < Estados; i++){
            tmp = document.getElementById("Estado_"+(i)).value;
            States.push(tmp);
        }

        for ( i = 0; i < Entradas; i++){
            tmp = document.getElementById("Entrada_"+(i)+"_"+"Entrada").value;
            Inputs.push(tmp);
        }

        for ( i = 0; i < Entradas; i++){
            Values[i] = [];
            Outputs[i] = [];
            for ( j = 0; j < Estados; j++){
                    tmp = document.getElementById((i)+"_"+(j)+"_"+"Entradas").value;
                    Values[i][j] = tmp;
                    tmp = document.getElementById((i)+"_"+(j)+"_"+"Salidas").value;
                    Outputs[i][j] = tmp;
            }
        }
        MatrixFlag = 1;

    for (var i = 0; i < Estados+1; i++){
    	Tabla_Completa[i] = [];
    	for (var j = 0; j < (Entradas*2)+1; j++){
    		Tabla_Completa[i][j] = 0;
    	}
    }
    
    for (var i = 0; i < Estados; i++){
    	Tabla_Completa[i+1][0] = States[i];
    }

	for (var i = 0; i < Entradas; i++){
    	Tabla_Completa[0][i+1] = Inputs[i];
    	Tabla_Completa[0][Entradas+i+1] = Inputs[i];

    }
    
    for (var i = 0; i < Entradas; i++){
    	for(var j = 0; j < Estados; j++){
    		Tabla_Completa[i+1][j+1] = Values[i][j];
    		Tabla_Completa[i+1][Entradas+j+1] = Outputs[i][j];
    	}
    }
    
	for (var i = 0; i < Fila; i++){
        if (document.getElementById('Radbtn'+i).checked == true){
            Estado_Inicial = States[i];
        }
    }

    }
}