#include <iostream>
#include <tuple>
using namespace std;

template <typename T>
class CSNode{
	private:
		int m_x;
		int m_y;
		T m_data;
	
	public:
		CSNode<T>* m_right;
		CSNode<T>* m_down;

		CSNode(){};

		CSNode(T data, int x, int y)
		{
			m_data = data;
			m_x = x;
			m_y = y;
			m_right = m_down = nullptr;
		};

		T get_data()
		{
			return m_data;
		}

		tuple<int, int, T> get_info()
		{
			return tuple<int, int, T>(m_x, m_y, m_data);
		}

		void set_right(CSNode<T>* right)
		{
			m_right = right;
		}

		void set_down(CSNode<T>* down)
		{
			m_down = down;
		}

		bool is_data(T data, int x, int y)
		{
			if ( data == m_data && x == m_x && y == m_y )
			{
				return 1;
			}
			return 0;
		}

		CSNode<T>* get_right()
		{
			return m_right;
		}

		CSNode<T>* get_down()
		{
			return m_down;
		}

};
