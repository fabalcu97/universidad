#include <iostream>
#include <stack>
#include "Node.h"

using namespace std;

template <typename T>
class BinTree
{

	public:

		CNode<T>* m_root;
		stack< CNode<T>** > m_pila;

		BinTree()
		{
			m_root = nullptr;
		}

		bool find(T data, CNode<T>**& p)
		{
			m_pila.push(p);
			for (p = &m_root; (*p) && (*p)->m_data != data; )
			{
				if ( data > (*p)->m_data )
				{
					p = &(*p)->m_right;
				}
				else
				{
					p = &(*p)->m_left;
				}
				m_pila.push(p);

			}
			return (*p) && (*p)->m_data == data;
		}

		bool insert(T data)
		{
			CNode<T>** p;

			while(!m_pila.empty())
			{
				m_pila.pop();
			}


			if (find(data, p))
			{
				return 0;
			}

			(*p) = new CNode<T>(data);
			cout<<endl<<"Inserté: "<<(*p)->m_data<<endl;
			fact_balance(m_root);
			balance();
			return 1;
		}

		int fact_balance( CNode<T>* p )
		{
			int r = 0;
			int l = 0;
			if (!p)
			{
				return 0;
			}
			r = fact_balance(p->m_left);
			l = fact_balance(p->m_right);

			if ( r > l )
			{
				p->m_height += r;
			}
			else
			{
				p->m_height += l;
			}
			if (p->m_left && p->m_right)
			{
				p->bal_fact = p->m_right->m_height - p->m_left->m_height;
			}
			if (p->m_left && !p->m_right)
			{
				p->bal_fact = 0 - p->m_left->m_height;
			}
			if (!p->m_left && p->m_right)
			{
				p->bal_fact = p->m_right->m_height;
			}

		}

		void balance()
		{
			stack< CNode<T>** > pila = m_pila;
			stack< CNode<T>** > pila1 = m_pila;
			CNode<T>** tmp;
			CNode<T>** tmp1;

			while(!pila1.empty())
			{
				cout<<pila1.top()<<"-";
				pila1.pop();
			}

			while(!pila.empty())
			{
				tmp = pila.top();
				
				if ( (*tmp)->bal_fact > 1 )
				{
					tmp1 = &(*tmp)->m_right;
					if ( (*tmp1)->bal_fact == 1 )
					{
						right_rotation(tmp);
					}
				}

				if ( (*tmp)->bal_fact < -1 )
				{
					tmp1 = &(*tmp)->m_left;
					if ( (*tmp1)->bal_fact == -1 )
					{
						right_rotation(tmp);
					}
				}
				pila.pop();
				fact_balance(m_root);
			}

		}

		void right_rotation(CNode<T>**& p)
		{

			CNode<T>** tmp = &(*p)->m_left;

			(*p)->m_left = (*tmp)->m_right;

			p = tmp;

		}

		void left_rotation(CNode<T>**& p)
		{

			CNode<T>** tmp = &(*p)->m_right;

			(*p)->m_right = (*tmp)->m_left;

			p = tmp;

		}

		void preorder(CNode<T>* p)
		{
			if (!p)
			{
				return;
			}
			preorder(p->m_left);
			cout<<p->m_data<<"-";
			preorder(p->m_right);
		}

};

