#include <iostream>

using namespace std;

template <typename T>
class CNode
{
	public:
		T m_data;
		CNode<T>* m_right;
		CNode<T>* m_left;
		int bal_fact;
		int m_height;

		CNode(T data)
		{
			m_data = data;
			m_height = 1;
			bal_fact = 0;
			m_right = m_left = nullptr;
		}
};
