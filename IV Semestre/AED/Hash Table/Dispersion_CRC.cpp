#include <string>
#include <iostream>
#include <vector>

using namespace std;

unsigned reverse(unsigned x) {
      x = ((x & 0x55555555) <<  1) | ((x >>  1) & 0x55555555);
      x = ((x & 0x33333333) <<  2) | ((x >>  2) & 0x33333333);
      x = ((x & 0x0F0F0F0F) <<  4) | ((x >>  4) & 0x0F0F0F0F);
      x = (x << 24) | ((x & 0xFF00) << 8) | ((x >> 8) & 0xFF00) | (x >> 24);
   return x;
}

unsigned int crc(string message) {
   int i, j;
   unsigned int byte, crc;

   i = 0;
   crc = 0xFFFFFFFF;
   while (message[i] != 0) {
      byte = message[i];
      byte = byte;
      for (j = 0; j <= 7; j++)
      {
         if ((int)(crc ^ byte) < 0)
              crc = (crc << 1) ^ 0x04C11DB7;
         else crc = crc << 1;
         byte = byte << 1;
      }
      i = i + 1;
   }
   return crc;
}


unsigned int crc(int id) {

   unsigned int   crc = 0xFFFFFFFF;

   for (int j = 0; j <= 7; j++) {
      if ((int)(crc ^ id) < 0){
         crc = (crc << 1) ^ 0x04C11DB7;      //Estándar que maximiza la detección de errores y minimiza las colisiones
                                             //IEEE 802.3 --> además es difícil encontrar uno perfecto   
      }                                      //x27 + x26 + x23 + x22 + x16 + x12 + x11 + x10 + x8 + x7 + x5 + x4 + x2 + x + 1
      else{
         crc = crc << 1;
      }
      id <<= 1;
   }
   
   return crc;
}

unsigned int fnv(int id)
{
   unsigned long int hash = 2166136261;
   id = reverse(id);

   for(int i = 0; i < 7; i++)
   {
      hash = hash ^ 1099511628211;
      hash = hash * id;
   }
   return hash;
}

// ------------------------------ main ---------------------------------

int main() {

   vector<string> mm = {"Sabido",
                        "es",
                        "que",
                        "las",
                        "lenguas",
                        "española",
                        "e",
                        "inglesa",
                        "por",
                        "más",
                        "se", 
                        "diferencien",
                        "tanto",
                        "la",
                        "y", 
                        "deriven",
                        "de",
                        "distintos",
                        "ramos"
                        "familia",
                        "indoeuropea",
                        "presentan",
                        "semejanzas",
                        "sorprendentes",
                        "el",
                        "léxico",
                        ",",
                        "en",
                        "pronunciación",
                        "a",
                        "i",
                        "o",
                        "u",};

   /*for(int i = 0; i < mm.size(); i++){
      unsigned int tmp = crc32a(mm[i]);
      for(int j = 0; j < mm.size(); j++){
         if(i == j){
            if (i ==  mm.size()-1)
            {
               break;
            }
            j++;
         }
         if ( tmp == crc32a(mm[j]) ){
            cout<<"Colisión: "<<mm[j]<<"-#: "<<tmp<<endl;
            cnt++;
         }
      }
   }*/

   int cnt = 0;
   int l = 0;
   srand(time(NULL));

   for(int w = 0; w < 50; w++){
      for(int i = 0; i < 1000; i++){
         int tmp = rand();
         unsigned int crc1 = crc( tmp );
         for(int j = 0; j < 1000; j++){
            int tmp1 = rand();
            if ( crc1 == crc( tmp1 ) ){
               /*cout<<"Colisión entre: "<<tmp<<" y "<<tmp1<<endl;*/
               cnt++;
            }
         }
         cout<<"Colisiones: "<<cnt<<"---Total: "<<1000<<endl;
         cnt = 0;
      }
      cout<<"Promedio: "<<cnt/1000<<endl;
   }


   /*for(int w = 0; w < mm.size(); w++){
         unsigned int tmp = fnv( 4 );
         cout<<"CRC= "<<tmp<<"----Mod= "<<tmp%29<<endl;
   }*/

   return 0;
}