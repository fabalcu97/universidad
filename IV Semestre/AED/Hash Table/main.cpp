#include <iostream>
#include "Hash_Table.h"
#include "Vector_Adaptor.h"
#include "Dispersion.h"

using namespace std;

struct Traits{
	typedef int T;
    typedef CDispersion_Modulo<T> Dispersion;
    typedef CVectorAdaptor<T> Adaptor;
};

struct Traits1{
	typedef int T;
    typedef CDispersion_FNV<T> Dispersion;
    typedef CVectorAdaptor<T> Adaptor;
};

struct Traits2{
	typedef int T;
    typedef CDispersion_CRC<T> Dispersion;
    typedef CVectorAdaptor<T> Adaptor;
};

int main(){

	srand(time(NULL));

	int n = 997;

	vector<string> mm = {"Sabido",
                        "es",
                        "que",
                        "las",
                        "lenguas",
                        "española",
                        "e",
                        "inglesa",
                        "por",
                        "más",
                        "se", 
                        "diferencien",
                        "tanto",
                        "la",
                        "y", 
                        "deriven",
                        "de",
                        "distintos",
                        "ramos"
                        "familia",
                        "indoeuropea",
                        "presentan",
                        "semejanzas",
                        "sorprendentes",
                        "el",
                        "léxico",
                        ",",
                        "en",
                        "pronunciación",
                        "a",
                        "i",
                        "o",
                        "u",};
	int 	fnv = 0,
			crc = 0,
			mod = 0;
	
	double	t_fnv,
			t_crc,
			t_mod;

	for(int j = 0; j < 1000; j++)
	{
		CHash_Table<Traits> Mod_Hash(n);
		CHash_Table<Traits1> FNV_Hash(n);
		CHash_Table<Traits2> CRC_Hash(n);
		
		clock_t tStart = clock();
			for (int i = 0; i < n; ++i)
			{
				Mod_Hash.Insert( rand() );
			}
	    t_mod += (double)(clock() - tStart) / CLOCKS_PER_SEC;
		mod = ( mod + Mod_Hash.vacios() ) & 2147483647;

	    clock_t tStart1 = clock();
			for (int i = 0; i < n; ++i)
			{
				FNV_Hash.Insert( rand() );
			}
	    t_fnv += (double)(clock() - tStart1) / CLOCKS_PER_SEC;
		fnv = ( fnv + FNV_Hash.vacios() ) & 2147483647;

	    clock_t tStart2 = clock();
			for (int i = 0; i < n; ++i)
			{
				CRC_Hash.Insert( rand() );
			}
	    t_crc += (double)(clock() - tStart2) / CLOCKS_PER_SEC;
		crc = (crc +  CRC_Hash.vacios() ) & 2147483647;
	}

	cout<<endl;
	cout<<"Tiempo MOD: "<<t_mod/1000<<endl;
	cout<<"Espacios Vacios MOD: "<<mod/1000<<endl;
	cout<<endl;
	cout<<"Tiempo FNV: "<<t_fnv/1000<<endl;
	cout<<"Espacios Vacios FNV: "<<fnv/1000<<endl;
	cout<<endl;
	cout<<"Tiempo CRC: "<<t_crc/1000<<endl;
	cout<<"Espacios Vacios CRC: "<<crc/1000<<endl;
	cout<<endl;

}
