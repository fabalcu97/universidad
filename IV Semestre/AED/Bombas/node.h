#ifndef NODE_H
#define NODE_H

#include <unistd.h>
#include <mutex>
#include <string>

using namespace std;

template <typename T>
class Node{
    public:

        bool decrease = false;

        Node(T info, int time){

            valor = info;
            tiempo = time;
            decrease = false;

        }
        mutex F;
        int valor;
        int tiempo;
        Node* Next;
        Node* Prev;
};

#endif // NODE_H
