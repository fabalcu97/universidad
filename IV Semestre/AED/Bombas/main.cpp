/////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include <thread>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <ncurses.h>

#include "Iterator.h"
#include "lista_circular.h"

using namespace std;

struct Traits{
    typedef int T;
    typedef C_Iterator_forward<T> I;
};

Lista_Circular<Traits> lista;
mutex F;


void Pantalla();

void Mover();

void Decrement();


int main(){


    int n, x, t;
    cout<<"Ingrese la cantidad de elementos: ";
    cin>>n;
    cout<<"Ingrese el tiempo por elemento: ";
    cin>>t;

    for(int i = 0; i < n; i++){
        cout<<"Ingrese el elemento número "<<i+1<<": ";
        cin>>x;
        lista.Insert(x, t);
    }

    Lista_Circular<Traits>::Iterator It;

    for(It = lista.begin(); It != lista.end(); It++){
        cout<<*It<<endl;
    }

    usleep(100000);

    initscr(); /* Start curses mode */
    raw(); /* Line buffering disabled */
    keypad(stdscr, TRUE); /* We get F1, F2 etc.. */
    noecho(); /* Don't echo() while we do getch */

    curs_set(0);

    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);
    init_pair(3, COLOR_CYAN, COLOR_BLACK);

    bkgd(COLOR_PAIR(1));



    /*int n, x, t;
    mvprintw(4, 4, "Ingrese la cantidad de elementos: ");
    refresh();
    cin>>n;
    printw("%d", n);
    refresh();
    move(4,4);          // move to begining of line
    clrtoeol();
    mvprintw(4, 4, "Ingrese el tiempo por elemento: ");
    refresh();
    cin>>t;
    printw("%d", t);
    refresh();
    move(4,4);          // move to begining of line

    for(int i = 0; i < n; i++){
        move(4,4);          // move to begining of line
        clrtoeol();
        mvprintw(4, 4, "Ingrese el elemento número %d: ", i+1);
        cin>>x;
        printw("%d", x);
        refresh();
        lista.Insert(x, t);
    }

    clrtoeol();
    Lista_Circular<Traits>::Iterator It;
    ofstream Texto("Print.txt");
    string l;

    std::string cmd = "gnome-terminal -x sh -c './Bombas; bash' ";
    system (cmd.c_str ());

    int i = 3;

    for(It = lista.begin(); It != lista.end().m_it->Next; i++, It++){
        l = to_string(*It);
        Texto<<l<<" - ";
    }
    Texto.close();*/
    refresh();

    getch();
    (*lista.m_iterator.m_it)->decrease = true;

    thread Thd(Mover);
    Pantalla();

    while(!lista.Empty()){
        Decrement();
    }

    Thd.join();

    clear();
    mvprintw(7, 7, "Perdiste... =(");
    refresh(); /* Print it on to the real screen */
    getch(); /* Wait for user input */
    endwin(); /* End curses mode */
    return 0;

}


void Mover(){

    int input = 0;

    while (input != 'q' && !lista.Empty()){

        input = getch();

        if( lista.m_iterator == lista.end() ){
            break;
        }

        (*lista.m_iterator.m_it)->decrease = false;

        if(input == KEY_RIGHT){
            lista.m_iterator++;
        }

        if(input == KEY_LEFT){
            lista.m_iterator--;
        }

        (*lista.m_iterator.m_it)->decrease = true;
    }

}

void Pantalla(){

    typedef typename Traits::I Iterator;

    int* ID = &lista.ID;
    Iterator tmp(&lista.Head);

    clear();
    printw("\n\n\n\n          ");
    for(int i = 0; i < *ID; tmp++, i++){

        if( (*tmp.m_it)->decrease ){

            attron(COLOR_PAIR(2));
            printw(" <- (%d; %d) ->", *tmp, (*tmp.m_it)->tiempo);
            attroff(COLOR_PAIR(2));
        }
        else{
            printw(" <- (%d; %d) ->", *tmp, (*tmp.m_it)->tiempo);
        }

    }
    refresh();
}

void Decrement(){


    lista.m_iterator  = lista.begin();

    while ((*lista.m_iterator.m_it)->tiempo > 0 && (*lista.m_iterator.m_it)->decrease){
       (*lista.m_iterator.m_it)->tiempo--;
        usleep(1000000);
        Pantalla();
    }

    if((*lista.m_iterator.m_it)->tiempo <= 0){
        lista.m_iterator++;
        (*lista.m_iterator.m_it)->decrease = true;
        clear();

        attron(COLOR_PAIR(3));
        mvaddstr(6, 7,"Adiós =(");
        mvaddch(9,8,'\\');
        mvaddch(9,12,'/');
        mvaddch(9,10,'|');
        mvaddch(10,12,'-');

        mvprintw(10, 10, "%d", (*lista.m_iterator.m_it)->Prev->valor);

        mvaddch(10,8,'-');
        mvaddch(11, 8,'/');
        mvaddch(11,12,'\\');
        mvaddch(11,10,'|');
        refresh();

        attroff(COLOR_PAIR(3));
        usleep(1000000);
        clear();
        lista.Remove((*lista.m_iterator.m_it)->Prev->valor);
    }
}



















