using namespace std;

template<typename G>
class CNode{

	public:
		typedef typename G::N N;
		typedef typename G::Edge Edge;
		N m_data;
		vector< Edge* > m_edges;

		CNode(N _data, Edge* _edge)
		{
			m_data = _data;
			m_edges.push_back(_edge);
		}
		eraseEdge(Edge* _edge)
		{
			int i = 0;
			for (; m_edges[i] != _edge; i++);

		}
};