using namespace std;

template<typename G>
class CEdge{

	public:
		typedef typename G::E E;
		typedef typename G::Node Node;
		E m_data;
		Node* m_nodes[2];
		bool m_dir;

		CEdge(Node* _begin, Node* _end, int weight = 0)
		{
			m_nodes[0] = _begin;
			m_nodes[1] = _end;
			m_data = weight;
		}

		erase(){
			m_nodes[0] = m_nodes[1] = nullptr;
		}

};