#include <iostream>
#include <vector>
#include <mutex>
#include <thread>

using namespace std;

mutex F;
bool Sorted_flag = false;
vector<thread*> threads;
int NT = thread::hardware_concurrency();

template <typename T>
void Stupid_Sorting(vector<T>);

template <typename T>
bool Sorted(vector<T>*);

template <typename T>
void print(vector<T>*);

template <typename T>
void print(vector<T>);

template <typename T>
void Stupid_PSort(vector<T>*);

template <typename T>
void Stupid_Sort(vector<T>*);

int main(){

    vector<int> array = {1, 3, 4, 5, 67, 2, 4, 345, 465, 645
                        };
//    vector<int> array = {1, 3, 4, 5, 67, 2, 4, 345, 465, 425, 34};

//    NT = 10;
        clock_t start = clock();


//        Stupid_PSort(&array);
        Stupid_Sort(&array);

        cout <<"---"<<  (clock() - start) / (double)(CLOCKS_PER_SEC *10)<<"---\n";

        F.lock();
        cout<<"\n---"<<threads.size()<<"---"<<endl;
        F.unlock();


}

template <typename T>
void Stupid_Sort(vector<T>* _array){

    srand(time(NULL));
    int n = _array->size();

    while(!Sorted(_array)){
        for (int i = 0; i < n; i++){
            swap(_array->at(rand()%n), _array->at(rand()%n));
        }
    }
}



template <typename T>
void Stupid_PSort(vector<T>* _array){

    srand(time(NULL));

    while(!Sorted_flag){
//        if(threads.size() < NT)
        threads.push_back(new thread(Stupid_Sorting<int>, *_array));
    }
    for(int i = 0; i < threads.size(); i++){
        threads[i]->join();
    }
}

template <typename T>
void Stupid_Sorting(vector<T> _array){
    int n = _array.size();
    for (int i = 0; i < n; i++){
        swap(_array.at(rand()%n), _array.at(rand()%n));
    }
    if(Sorted(&_array)){
        Sorted_flag = true;
        print(_array);
    }
}



template <typename T>
bool Sorted(vector<T>* _array){
    int n = _array->size();
    for (int i = 1; i < n; i++){
        if(_array->at(i-1) > _array->at(i)){
            return false;
        }
    }
    return true;
}







template <typename T>
void print(vector<T> a){

    int n = a.size();

    for (int i = 0; i < n; i++){
        cout << a[i] << "-";
    }
    cout<<endl;

}

template <typename T>
void print(vector<T>* _array){
    int n = _array->size();
    for (int i = 0; i < n; i++){
        cout<<*(_array->begin()+i)<<"-";
    }
    cout<<endl;
}
