#ifndef BUBBLE_SORT_H
#define BUBBLE_SORT_H

#include <vector>

using namespace std;

template <typename T>
void print(vector<T> a){

    int n = a.size();

    for (int i = 0; i < n; i++){
        cout << a[i] << "-";
    }
    cout<<endl;

}

template <typename T>
vector<T> Bubble_sort(vector<T> _array){

    int n = _array.size();
    bool swapped = true;

    while (swapped){
        swapped = false;
        for (int j = 1; j < n; j++){
            if (_array[j-1] > _array[j]){

                swap(_array[j-1], _array[j]);
                swapped = true;
            }
        }
    }
    return _array;
}


void Bubble_PSort(vector<int>* _array){

    int n = _array->size();
    bool swapped = true;

    while (swapped){
        swapped = false;
        for (int j = 0; j < n; j++){

            if ( *(_array->begin()+(j-1)) > *(_array->begin()+j)){

                swap(*(_array->begin()+(j-1)), *(_array->begin()+(j)));

                swapped = true;
            }
        }
    }
}


vector<int> Merge(vector<int>::iterator a1, vector<int>::iterator b1, vector<int>::iterator a2, vector<int>::iterator b2){

    vector<int> result;

    while (a1 != a2 && b1 != b2){
        if (*a1 < *b1){
            result.push_back(*a1);
            a1++;
        }
        else{
            result.push_back(*b1);
            b1++;
        }
    }
    if(a1 == a2){
        result.insert(result.end(), b1, b2);
    }
    else{
        result.insert(result.end(), a1, a2);
    }
    return result;
}


#endif // BUBBLE_SORT_H













