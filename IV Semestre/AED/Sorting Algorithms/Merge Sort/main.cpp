#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include "Merge.h"

using namespace std;

int min(int, int);
void Merge(vector<int>*, int, int, int);
void Merge_Sort(vector<int>* _array);
void Merge_PSort(vector<int>* _array);

mutex M;
vector<thread*> Threads;
int NT;

int main(){

    srand(time(NULL));

    vector<int> array;

    int n;
    double x;

    int elementos[6] = {10, 100, 1000, 10000, 100000, 10000000};

    NT = 100;

    for(int i = 0; i < 6; i++){

        n = 1000;

        for (int i = 0; i < n; i++){
            array.push_back(rand() % n);
        }

        cout<<"--------------------\n";

        clock_t start = clock();

        Merge_Sort(&array);
//        Merge(&array, 0, array.size()-1, array.size()/2);

        x = (clock() - start) / (double)(CLOCKS_PER_SEC);

        orden(&array);

        if(flag){
            cout<<"Ordenado";
        }

        print(array);

        cout<<endl<<"Tiempo: "<<x<<"\nHilos: "<<NT<<"\nCantidad Elementos: "<<n<<endl<<"--------------------\n";

        flag = false;

        array.clear();
//        getchar();
    }

}


int min(int a, int b){
    if (a > b){
        return b;
    }
    return a;
}



void Merge_Sort(vector<int>* _array){

    int n = _array->size();

    for(int i = 1; i <= n-1; i = 2*i){                          ///Define el tamaño del vector a ordenar, de cuanto en cuanto

        for(int j = 0; j < n-1; j += 2*i){                      ///Donde comienza el vector de tamaño i
//            cout<<"En "<<j<<" comienza bloque de "<<i<<endl;

            Merge(_array, j, min(j+2*i-1, n-1), i+j-1);         ///Mezcla de manera ordenada los vectores de tamaño i
        }
    }
}


void Merge_PSort(vector<int>* _array){

    int n = _array->size();
    int med, fin;

    while(!flag){
        for(int i = 1; i <= n-1; i = 2*i){                          ///Define el tamaño del vector a ordenar, de cuanto en cuanto

            for(int j = 0; j < n-1; j += 2*i){                      ///Donde comienza el vector de tamaño i
                med = i+j-1;
                fin = min(j+2*i-1, n-1);
//                if(Threads.size() < NT)
                Threads.push_back(new thread (Merge, _array, j, fin, med));
            }
//            for (int i = 0; i < Threads.size(); i++){
//                Threads[i]->join();
//            }

        }

        orden(_array);
    }
}


void Merge(vector<int>* _array, int ini, int fin, int med){
    int i = 0,
        j = 0,
        k = ini,
        tm1 = med - ini + 1,
        tm2 = fin - med,
        n = _array->size();

    vector<int> tmp1,
                tmp2;

    for (int i = 0; i < tm1 && (ini+i) < n; i++){
        tmp1.push_back(_array->at(ini+i));
    }
    for (int i = 0; i < tm2 && (med+i-1) < n; i++){
        tmp2.push_back(_array->at(med+i+1));
    }

//    print(tmp1);
//    print(tmp2);
//    cout<<endl;

    while (i < tm1 && j < tm2){
        M.lock();
        if (tmp1[i] <= tmp2[j])
        {
            *(_array->begin()+k) = tmp1[i];
            i++;
        }
        else
        {
            *(_array->begin()+k) = tmp2[j];
            j++;
        }
        k++;
        M.unlock();
    }
    M.lock();
    while (i < tm1)
    {
        *(_array->begin()+k) = tmp1[i];
        i++;
        k++;
    }

    while (j < tm2)
    {
        *(_array->begin()+k) = tmp2[j];
        j++;
        k++;
    }
    M.unlock();
//    print(_array);
    return;

}








