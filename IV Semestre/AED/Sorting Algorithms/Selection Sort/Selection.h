#ifndef SELECTION_H
#define SELECTION_H

#include <vector>

using namespace std;

mutex ff;


template <typename T>
void print(vector<T> _array){
    int n = _array.size();
    for (int i = 0; i < n; i++){
        cout<<_array[i]<<"-";
    }
    cout<<endl;
}

template <typename T>
void printP(vector<T>* _array){
    int n = _array->size();
    for (int i = 0; i < n; i++){
        cout<<*(_array->begin()+i)<<"-";
    }
    cout<<endl;
}



bool flag = false;

void verify(vector<int>* _array){
    int n = _array->size();
    for (int i = 1; i < n; i++){
        if(_array->at(i-1) > _array->at(i)){
            flag = false;
            return;
        }
    }
    flag = true;
}


void orden(vector<int>* _array){

    int n = _array->size();

    vector<int> _array1(_array->begin(), _array->begin() + (n/4));

    vector<int> _array2(_array->begin() + ((n/4)+1), _array->begin() + n/2);

    vector<int> _array3(_array->begin() + ((n/2)+1), _array->begin() + 3*(n/4));

    vector<int> _array4(_array->begin() + (3*(n/4)+1), _array->end());


    thread T1(verify, &_array1);
    thread T2(verify, &_array2);
    thread T3(verify, &_array3);
    thread T4(verify, &_array4);

    T1.join();
    T2.join();
    T3.join();
    T4.join();

}

template <typename T>
void Selection_Sort(vector<T>* _array){

    int     n = _array->size(), aux;
    T       tmp;

    for (int j = 0; j < n; j++){

        tmp = *(_array->begin() + j);               ///Hace swaps entre el elemento [j] con [i] donde [i] es el menor siguiente
        aux = 0;
        for (int i = j; i < n; i++){

            if ( *(_array->begin()+i) < tmp ){
                tmp = *(_array->begin()+i);
                aux = i;
            }

        }
        if (aux != 0){
            swap( *(_array->begin() + j), *(_array->begin()+aux) );
        }
    }

}


vector<int> Merge(vector<int>::iterator a1, vector<int>::iterator a2, vector<int>::iterator b1, vector<int>::iterator b2){

    vector<int> result;

    while (a1 != a2 && b1 != b2){
        if (*a1 < *b1){
            result.push_back(*a1);
            a1++;
        }
        else{
            result.push_back(*b1);
            b1++;
        }
    }
    if(a1 == a2){
        result.insert(result.end(), b1, b2);
    }
    else{
        result.insert(result.end(), a1, a2);
    }
    return result;
}



#endif // SELECTION_H
