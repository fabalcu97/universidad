#ifndef RBTREE_H
#define RBTREE_H

#include <iostream>
#include <iomanip>
#include <stack>

#include "rbnode.h"
#include "concolor.h"

using namespace std;

#define BLACK 0
#define RED 1

template <typename Tr>
class CRBTree{

    typedef typename Tr::T T;

    stack< CRBNode<T>* > m_pila;

    public:
	    CRBNode<T>* m_root;
        
        CRBTree(){
            m_root = nullptr;
        }


        bool find(T x, CRBNode<T>**& p){
            
            while (!m_pila.empty()){
                m_pila.pop();
            }
            for (p = &m_root; (*p) && (*p)->m_data != x; ){
                m_pila.push(*p);
                /*cout<<"Pasé por: "<<(*p)->m_data<<endl;*/
                if ( x < (*p)->m_data )
                {
                    p = &(*p)->m_left;
                }
                else if ( x > (*p)->m_data )
                {
                    p = &(*p)->m_right;
                }
            }
            return (*p);
        }

        bool insert(T x){
            CRBNode<T>** p;
            if (find(x, p)) {
                return 0;
            }
            *p = new CRBNode<T>(x);
            m_pila.push(*p);
            if ( (*p) == m_root )
            {
                m_root->m_color = BLACK;
            }
            cout<<"Inserté: "<<(*p)->m_data<<endl;
            insertion_balance();
            return 1;
        }

		void insertion_balance(){
            
            /*print(m_root, 4);
            cout<<"--------Balancea-------\n";*/

            int cnt = 0;

            stack< CRBNode<T>* > pila = m_pila;
            CRBNode<T>* tmp;
            CRBNode<T>* tmp1;

            while( !pila.empty() )
            {
                tmp = pila.top();
                pila.pop();
                cnt++;

                /*cout<<"tmp: "<<tmp->m_data<<endl;*/
                
                if ( tmp == m_root )
                {
                    m_root->m_color = BLACK;
                }

                if (cnt >= 3)
                {
                    /*cout<<"Holi"<<endl;*/
                    if( tmp->m_right && tmp->m_left && tmp->m_left->m_color == RED && tmp->m_right->m_color == RED )
                    {
                        tmp->m_right->m_color = BLACK;
                        tmp->m_left->m_color = BLACK;
                        tmp->m_color = RED;
                        if ( tmp == m_root )
                        {
                            m_root->m_color = BLACK;
                        }
                    }
                    else
                    {
                        if ( tmp->m_right && tmp->m_right->m_right && tmp->m_right->m_color == RED && tmp->m_right->m_right->m_color == RED )
                        {
                            tmp1 = tmp->m_right;
                            rotate_left(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp->m_color = RED;
                            tmp1->m_color = BLACK;
                        }
                        if ( tmp->m_right && tmp->m_right->m_left && tmp->m_right->m_color == RED && tmp->m_right->m_left->m_color == RED )
                        {
                            tmp1 = tmp->m_right->m_left;
                            rotate_right(tmp->m_right);
                            tmp->m_right = tmp1;
                            
                            rotate_left(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp->m_color = RED;
                            tmp1->m_color = BLACK;
                        }
                        if ( tmp->m_left && tmp->m_left->m_left && tmp->m_left->m_color == RED && tmp->m_left->m_left->m_color == RED )
                        {
                            tmp1 = tmp->m_left;
                            rotate_right(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp->m_color = RED;
                            tmp1->m_color = BLACK;
                        }
                        if ( tmp->m_left && tmp->m_left->m_right && tmp->m_left->m_color == RED && tmp->m_left->m_right->m_color == RED )
                        {
                            tmp1 = tmp->m_left->m_right;
                            rotate_left(tmp->m_left);
                            tmp->m_left = tmp1;

                            rotate_right(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp->m_color = RED;
                            tmp1->m_color = BLACK;
                        }
                    }
                }
            }
            cnt = 0;
            /*cout<<"Salí del balanceo"<<endl;*/
        }

        void rotate_left(CRBNode<T>* p){

            CRBNode<T>* tmp;
            tmp = p->m_right;
            if(p == m_root){
                m_root = tmp;
            }
            if( tmp->m_left ){
                p->m_right = tmp->m_left;
            }
            else{
                p->m_right = nullptr;
            }
            tmp->m_left = p;
        }

        void rotate_right(CRBNode<T>* p){

            CRBNode<T>* tmp;
            tmp = p->m_left;
            if(p == m_root){
                m_root = tmp;
            }
            if( tmp->m_right ){
                p->m_left = tmp->m_right;
            }
            else{
                p->m_left = nullptr;
            }
            tmp->m_right = p;
        }

        void print(CRBNode<T>* p, int indent){
        
            if (p != NULL){

                if(p->m_right){

                    print(p->m_right, indent+4);

                }
                if (indent){
                    cout<<setw(indent)<<' ';
                }
                if(p->m_right){
                    cout<<"/"<<endl<<setw(indent);
                }
                if ( p->m_color == RED )
                {
                    /*cout<<red<<p->m_data<<normal<<endl;*/
                    cout<<p->m_data<<"(Red)"<<endl;
                }
                if ( p->m_color == BLACK )
                {
                    /*cout<<yellow<<p->m_data<<normal<<endl;*/
                	cout<<p->m_data<<"(Black)"<<endl;
                }
                if (p->m_left){

                    cout<<setw(indent+3)<<"\\\n";
                    print(p->m_left, indent+4);

                }

            }
        }

        bool remove(T x){

            
            CRBNode<T>** p;

            if (!find(x, p)){
                return 0;
            }

            CRBNode<T>* tmp = (*p);

            if ( ((*p)->m_left)  &&  ((*p)->m_right) ){
                m_pila.push(*p);
                /*cout<<"Ingresó a la pila: "<<(*p)->m_data<<endl;*/
                for( p = &(*p)->m_right; (*p)->m_left; p = &(*p)->m_left){
                    m_pila.push(*p);
                    /*cout<<"Ingresó a la pila: "<<(*p)->m_data<<endl;*/
                }
                tmp->m_data = (*p)->m_data;
                tmp = (*p);
            }
            if(!((*p)->m_left)){
            /*    m_pila.push(*p);
                cout<<"Ingresó a la pila: "<<(*p)->m_data<<endl;*/
                (*p) = (*p)->m_right;
            }
            else{
                /*m_pila.push(*p);
                cout<<"Ingresó a la pila: "<<(*p)->m_data<<endl;*/
                (*p) = (*p)->m_left;
            }
            /*stack< CRBNode<T>* > pila1 = m_pila;
            while(!pila1.empty())
            {
                CRBNode<T>* sss= pila1.top();
                cout << sss->m_data;
                pila1.pop();
            }
            cout<<endl;*/
            remove_balance(tmp);
            return 1;
        }

        void remove_balance(CRBNode<T>* p)
        {
            /*cout<<"p: "<<p->m_data<<endl;*/
            stack< CRBNode<T>* > pila = m_pila;
            CRBNode<T>* tmp;
            CRBNode<T>* tmp1;

            stack< CRBNode<T>* > pila1 = m_pila;
            while(!pila1.empty())
            {
                CRBNode<T>* sss= pila1.top();
                cout << sss->m_data;
                pila1.pop();
            }
            cout<<endl;

            if (p->m_color == RED || (p->m_left && p->m_left->m_color == RED) || (p->m_right && p->m_right->m_color == RED))
            {
                if(p->m_left && p->m_left->m_color == RED)
                {
                    p->m_left->m_color = BLACK;
                }
                if(p->m_right && p->m_right->m_color == RED)
                {
                    p->m_right->m_color = BLACK;
                }
                delete p;
                return;
            }
            delete p;
            /*if(!pila.empty())
            {
                pila.pop();
            }*/
            
            /*pila1 = pila;
            while(!pila1.empty())
            {
                CRBNode<T>* sss= pila1.top();
                cout << sss->m_data;
                pila1.pop();
            }
            cout<<endl;*/

            while(!pila.empty())
            {
                tmp = pila.top();
                pila.pop();

                /*cout<<"tmp: "<<tmp->m_data<<endl;
                cout<<"Holi111"<<endl;*/
                if ( (!tmp->m_right && tmp->m_left->m_color == BLACK) || (!tmp->m_left && tmp->m_right->m_color == BLACK) )     //Deleted sibling BLACK case
                {
                    if ( !tmp->m_left && tmp->m_right->m_color == BLACK )
                    {   
                        tmp1 = tmp->m_right;
                        if ( ( tmp1->m_right && tmp1->m_left ) && ( tmp1->m_right->m_color == BLACK && tmp1->m_left->m_color == BLACK ) )//Sibling and sibling's sons BLACK
                        {
                            if ( tmp->m_color == BLACK )
                            {
                                tmp1->m_color = RED;
                            }
                        }
                        else if ( tmp1->m_right && tmp1->m_right->m_color == RED )       //Single Left Rotation
                        {
                            rotate_left(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp1->m_right->m_color = BLACK;
                            tmp1->m_left->m_color = BLACK;
                            tmp1->m_color = RED;
                        }
                        else if ( tmp1->m_left && tmp1->m_left->m_color == RED )    //Right Left Rotation
                        {
                            tmp1 = tmp1->m_left;
                            rotate_right(tmp->m_right);
                            tmp->m_right = tmp1;
                            
                            rotate_left(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp1->m_right->m_color = BLACK;
                            tmp1->m_left->m_color = BLACK;
                            tmp1->m_color = BLACK;
                        }
                    }
                    else if( tmp->m_left->m_color == BLACK && !tmp->m_right )
                    {
                        tmp1 = tmp->m_left;
                        if ( ( tmp1->m_right && tmp1->m_left ) && ( tmp1->m_right->m_color == BLACK && tmp1->m_left->m_color == BLACK ) )//Sibling and sibling's sons BLACK
                        {
                            if ( tmp->m_color == BLACK )
                            {
                                tmp1->m_color = RED;
                            }
                        }
                        else if( tmp1->m_left && tmp1->m_left->m_color == RED)           //Single Right Rotation
                        {
                            rotate_right(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp1->m_right->m_color = BLACK;
                            tmp1->m_left->m_color = BLACK;
                            tmp1->m_color = BLACK;
                        }
                        else if( tmp1->m_right && tmp1->m_right->m_color == RED)    //Left Right Rotation
                        {
                            tmp1 = tmp1->m_right;
                            rotate_left(tmp->m_left);
                            tmp->m_left = tmp1;

                            rotate_right(tmp);
                            if(!pila.empty()){
                                if (pila.top()->m_right == tmp){
                                    pila.top()->m_right = tmp1;
                                }
                                else{
                                    pila.top()->m_left = tmp1;   
                                }
                            }
                            tmp1->m_right->m_color = BLACK;
                            tmp1->m_left->m_color = BLACK;
                            tmp1->m_color = BLACK;
                        }
                    }
                }
                else if ( (!tmp->m_right && tmp->m_left->m_color == RED ) || ( !tmp->m_left && tmp->m_right->m_color == RED) )     //Deleted sibling BLACK case
                {
                    if (!tmp->m_right && tmp->m_left->m_color == RED )
                    {
                        tmp1 = tmp->m_left;
                        /*cout<<"TMP1: "<<tmp1->m_data<<endl;*/
                        rotate_right(tmp);
                        if(!pila.empty()){
                            /*cout<<"TMP1: "<<tmp1->m_data<<endl;*/
                            if (pila.top()->m_right == tmp){
                                pila.top()->m_right = tmp1;
                            }
                            else{
                                pila.top()->m_left = tmp1;   
                            }
                        }
                        if(tmp1->m_right) tmp1->m_right->m_color = BLACK;
                        if(tmp1->m_left) tmp1->m_left->m_color = BLACK;
                        tmp1->m_color = BLACK;
                        if(tmp->m_left)
                        {
                            tmp->m_left->m_color = RED;
                        }
                    }
                    else if ( !tmp->m_left && tmp->m_right->m_color == RED)
                    {
                        tmp1 = tmp->m_right;
                        /*cout<<"TMP1: "<<tmp1->m_data<<endl;*/
                        rotate_left(tmp);
                        if(!pila.empty()){
                            if (pila.top()->m_right == tmp){
                                pila.top()->m_right = tmp1;
                            }
                            else{
                                pila.top()->m_left = tmp1;   
                            }
                        }
                        tmp1->m_right->m_color = BLACK;
                        tmp1->m_left->m_color = BLACK;
                        tmp1->m_color = BLACK;
                        if(tmp->m_right)
                        {
                            tmp->m_right->m_color = RED;
                        }
                    }
                }/*
                cout<<"Holi"<<endl;
                print(m_root, 4);
                cout<<"-----------------\n\n\n";*/
            }
        }

};


#endif // RBTREE_H