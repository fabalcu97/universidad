#ifndef NODE_H
#define NODE_H

using namespace std;

template <typename T>
class CNode{

    private:
    public:
        
        T m_data;
        int m_balance = 0;
        int m_height = 1;

        CNode<T>* m_child[2];
        
        CNode(T x){
            m_data = x;
            m_child[0] = m_child[1] = nullptr;
        }

};


#endif // NODE_H
