#include <iostream>

#include "Node.h"
#include "Comparator.h"
#include "Iterator_inorder.h"
#include "Iterator_postorder.h"
#include "Iterator_preorder.h"
#include "Binary_Tree.h"

using namespace std;

struct Traits{
    typedef int T;
    typedef Comp_Less<T> Ht;
    typedef CIterator_i<T> Inorder;
    typedef CIterator_p<T> Postorder;
    typedef CIterator_po<T> Preorder;
};

int main(){

    srand(time(NULL));
    CBinTree<Traits> Tree;
    int elemento;
    char opt;

    srand(time(NULL));

    cout<<"-------AVL-------"<<endl;
    cout<<"a) Insertar\n";
    cout<<"b) Eliminar\n";
    cout<<"c) Imprimir Árbol\n";
    cout<<"s) Salir\n";
    cout<<"\nIngrese su opción: ";
    cin>>opt;

    do{
        if (opt == 'a'){
            cout<<"\nIngrese el elemento a insertar: ";
            cin>>elemento;
            Tree.insert(elemento);
            cout<<endl;
            Tree.print(Tree.m_root, 4);
            cout<<endl;
        }
        if (opt == 'b'){
            cout<<"\nIngrese el elemento a eliminar: ";
            cin>>elemento;
            cout<<"holi"<<"-";
            Tree.remove(elemento);
            cout<<endl;
            Tree.print(Tree.m_root, 4);
            cout<<endl;
        }
        if (opt == 'c'){
            cout<<endl;
            Tree.print(Tree.m_root, 4);
            cout<<endl;
        }
        if (opt == 'd'){
            for(int i = 0; i < 30; i++){
                Tree.insert(rand()%100);
            }
            cout<<endl;
            Tree.print(Tree.m_root, 4);
            cout<<endl;
        }

        cout<<endl;
        cout<<"-------AVL-------"<<endl;
        cout<<"a) Insertar\n";
        cout<<"b) Eliminar\n";
        cout<<"c) Imprimir Árbol\n";
        cout<<"s) Salir\n";
        cout<<"\nIngrese su opción: ";
        cin>>opt;

    }while(elemento != 's');
}


