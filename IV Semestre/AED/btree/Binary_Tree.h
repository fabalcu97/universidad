#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <iostream>
#include <vector>
#include <queue>
#include <iomanip>

using namespace std;

template <typename Tr>
class CBinTree{

    public:

        typedef typename Tr::T T;
        typedef typename Tr::Ht Cmp;
        typedef typename Tr::Inorder I_inorder;
        typedef typename Tr::Postorder I_postorder;
        typedef typename Tr::Preorder I_preorder;

        CNode<T>* m_root;
        stack< CNode<T>* > m_pila;

        Cmp Compare;

        CBinTree(){
            m_root = nullptr;
        }

/**
**
**
**  Recorridos
**
**
**/
        void amplitud(){
            queue<CNode<T>*> cola;
            cola.push(m_root);
            CNode<T>* tmp;
            while(!cola.empty()){
                tmp = cola.front();
                cout<<tmp->m_data<<"-";
                if(tmp->m_child[0]) cola.push(tmp->m_child[0]);
                if(tmp->m_child[1]) cola.push(tmp->m_child[1]);
                cola.pop();
            }
        }

        void profundidad(){
            
            stack<CNode<T>*> pila;
            pila.push(m_root);
            CNode<T>* tmp;

            while(!pila.empty()){
                tmp = pila.top();
                cout<<tmp->m_data<<"-";
                pila.pop();
                if(tmp->m_child[0]) pila.push(tmp->m_child[0]);
                if(tmp->m_child[1]) pila.push(tmp->m_child[1]);
            }
        }

        ///////////////////////// INORDER

        I_inorder begin_inorder(){
            CNode<T>* p = m_root;
            stack< pair<CNode<T>*, int> > Pila;
            pair<CNode<T>*, int> tmp;
            
            while(p){
                tmp.first = p;
                tmp.second = 1;
                Pila.push(tmp);
                p = p->m_child[0];
            }
            Pila.top().second++;
            return I_inorder(Pila);
        }

        I_inorder end_inorder(){
            stack< pair<CNode<T>*, int> > Pila;
            return I_inorder(Pila);
        }

        void inorder(){
            for(I_inorder i = begin_inorder(); i != end_inorder(); i++){
                cout<<"("<<*i<<")-";
            }
        }

        /////////////////////////
        ///////////////////////// POSTORDER

        I_postorder begin_postorder(){
            CNode<T>* p = m_root;
            stack< pair<CNode<T>*, int> > Pila;
            pair<CNode<T>*, int> tmp;
            
            while(p){
                tmp.first = p;
                tmp.second = 1;
                Pila.push(tmp);
                p = p->m_child[0];
            }
            return I_postorder(Pila);
        }

        I_postorder end_postorder(){
            stack< pair<CNode<T>*, int> > Pila;
            return I_postorder(Pila);
        }

        void postorder(){
            for(I_postorder i = begin_postorder(); i != end_postorder(); i++){
                cout<<"("<<*i<<")-";
            }
        }

        void r_postorder(CNode<T>* p){
            if (!p) return;
            r_postorder(p->m_child[0]);
            r_postorder(p->m_child[1]);
            cout<<p->m_data<<"-";
        }

        /////////////////////////
        ///////////////////////// PREORDER
        
        I_preorder begin_preorder(){
            stack< pair<CNode<T>*, int> > Pila;
            pair<CNode<T>*, int> tmp;            
            tmp.first = m_root;
            tmp.second = 1;
            Pila.push(tmp);
            return I_preorder(Pila);
        }

        I_preorder end_preorder(){
            stack< pair<CNode<T>*, int> > Pila;
            return I_preorder(Pila);
        }

        void preorder(){
            for(I_preorder i = begin_preorder(); i != end_preorder(); i++){
                cout<<"("<<*i<<")-";
            }
        }
        /////////////////////////
        /////////////////////////


        void balance_factor(){

            stack < CNode<T>* > pila = m_pila;

            CNode<T>* tmp;
            
            while(!pila.empty())
            {
                tmp = pila.top();
                if ( !tmp->m_child[0] && !tmp->m_child[1]){
                        tmp->m_balance = 0;
                        tmp->m_height = 1;
                    }
                else{
                    if (!tmp->m_child[1]){
                        tmp->m_balance = 0 - tmp->m_child[0]->m_height;
                        tmp->m_height = tmp->m_child[0]->m_height + 1;

                    }
                    else if (!tmp->m_child[0]){
                        tmp->m_balance = tmp->m_child[1]->m_height;
                        tmp->m_height = tmp->m_child[1]->m_height + 1;
                    }
                    else{
                        
                        tmp->m_balance = tmp->m_child[1]->m_height - tmp->m_child[0]->m_height;
                        
                        if(tmp->m_child[1]->m_height > tmp->m_child[0]->m_height){
                            tmp->m_height = tmp->m_child[1]->m_height + 1;
                        }
                        else{
                            tmp->m_height = tmp->m_child[0]->m_height + 1;
                        }
                    }
                    
                }
                pila.pop();
            }
        }

        bool remove(T x){

            
            CNode<T>** p;

            if (!find(x, p)){
                return 0;
            }

            CNode<T>* tmp = (*p);

            if ( ((*p)->m_child[0])  &&  ((*p)->m_child[1]) ){

                for( p = &(*p)->m_child[1]; (*p)->m_child[0]; p = &(*p)->m_child[0]);
                tmp->m_data = (*p)->m_data;
                tmp = (*p);
            }
            (*p) = (*p)->m_child[ !((*p)->m_child[0]) ];
            delete tmp;
            balance();
            return 1;
        }

        bool find(T x, CNode<T>**& p){
            
            while (!m_pila.empty()){
                m_pila.pop();
            }
            for (p = &m_root; (*p) && (*p)->m_data != x; p = &(*p)->m_child[Compare.compare((*p)->m_data, x)]){
                m_pila.push(*p);
            }            
            return (*p);
        }

        bool find(T x){
            CNode<T>** p;
            for (p = &m_root; (*p) && (*p)->m_data != x; p = &(*p)->m_child[Compare.compare((*p)->m_data, x)])
            if((*p)->m_child[0]->m_data == x || (*p)->m_child[1]->m_data == x ){
                return 1;
            }
            return 0;
        }

        bool insert(T x){
            CNode<T>** p;
            if (find(x, p)) {
                return 0;
            }
            *p = new CNode<T>(x);
            balance();
            return 1;
        }

        void rotate_left(CNode<T>* p){

            CNode<T>* tmp;
            tmp = p->m_child[1];
            if(p == m_root){
                m_root = tmp;
            }
            if( tmp->m_child[0] ){
                p->m_child[1] = tmp->m_child[0];
            }
            else{
                p->m_child[1] = nullptr;
            }
            tmp->m_child[0] = p;

        }

        void rotate_right(CNode<T>* p){

            CNode<T>* tmp;
            tmp = p->m_child[0];
            if(p == m_root){
                m_root = tmp;
            }
            if( tmp->m_child[1] ){
                p->m_child[0] = tmp->m_child[1];
            }
            else{
                p->m_child[0] = nullptr;
            }
            tmp->m_child[1] = p;

        }

        void balance(){
            
            stack< CNode<T>* > pila = m_pila;
            CNode<T>* tmp;
            CNode<T>* tmp1;

            balance_factor();

            while(!pila.empty()){

                tmp = pila.top();
                pila.pop();

                if (tmp->m_balance > 1){

                    if (tmp->m_child[1]->m_balance == -1){

                        tmp1 = tmp->m_child[1]->m_child[0];
                        rotate_right(tmp->m_child[1]);
                        tmp->m_child[1] = tmp1;
                        
                        rotate_left(tmp);
                        if(!pila.empty()){
                            if (pila.top()->m_child[1] == tmp){
                                pila.top()->m_child[1] = tmp1;
                            }
                            else{
                                pila.top()->m_child[0] = tmp1;   
                            }
                        }
                    }                    
                    else if(tmp->m_child[1]->m_balance == 0){
                        
                        tmp1 = tmp->m_child[1];
                        rotate_left(tmp);

                        if(!pila.empty()){
                            if (pila.top()->m_child[1] == tmp){
                                pila.top()->m_child[1] = tmp1;
                            }
                            else{
                                pila.top()->m_child[0] = tmp1;   
                            }
                        }
                    }
                    else if (tmp->m_child[1]->m_balance == 1){
                        tmp1 = tmp->m_child[1];
                        rotate_left(tmp);
                        if(!pila.empty()){
                            if (pila.top()->m_child[1] == tmp){
                                pila.top()->m_child[1] = tmp1;
                            }
                            else{
                                pila.top()->m_child[0] = tmp1;   
                            }
                        }
                    }
                }
                else if (tmp->m_balance < -1){
                    if (tmp->m_child[0]->m_balance == -1){

                        tmp1 = tmp->m_child[0];
                        rotate_right(tmp);
                        if(!pila.empty()){
                            if (pila.top()->m_child[1] == tmp){
                                pila.top()->m_child[1] = tmp1;
                            }
                            else{
                                pila.top()->m_child[0] = tmp1;   
                            }
                        }
                    }
                    else if (tmp->m_child[0]->m_balance == 0){
                        tmp1 = tmp->m_child[0];
                        rotate_right(tmp);

                        if(!pila.empty()){
                            if (pila.top()->m_child[1] == tmp){
                                pila.top()->m_child[1] = tmp1;
                            }
                            else{
                                pila.top()->m_child[0] = tmp1;   
                            }
                        }
                    }
                    else if (tmp->m_child[0]->m_balance  == 1){
                        tmp1 = tmp->m_child[0]->m_child[1];
                        rotate_left(tmp->m_child[0]);
                        tmp->m_child[0] = tmp1;

                        rotate_right(tmp);
                        if(!pila.empty()){
                            if (pila.top()->m_child[1] == tmp){
                                pila.top()->m_child[1] = tmp1;
                            }
                            else{
                                pila.top()->m_child[0] = tmp1;   
                            }
                        }
                    }
                }
                balance_factor();
            }
        }

    void print(CNode<T>* p, int indent){
        
        if (p != NULL){

            if(p->m_child[1]){

                print(p->m_child[1], indent+4);

            }
            if (indent){
                cout<<setw(indent)<<' ';
            }
            if(p->m_child[1]){
                cout<<"/"<<endl<<setw(indent);
            }

            cout<<p->m_data<<"("<<p->m_balance<<"-"<<p->m_height<<")"<<endl;
            if (p->m_child[0]){

                cout<<setw(indent+3)<<"\\\n";
                print(p->m_child[0], indent+4);

            }

        }
    }
};


#endif // BINARY_TREE_H



/*
if ((*tmp)->m_balance > 1){

                    if ((*tmp)->m_child[1]->m_balance == -1){
                        tmp1 = (*tmp)->m_child[1];
                        (*tmp)->m_child[1] = tmp1->m_child[0];
                        if ( !((*tmp)->m_child[1]->m_child[1]) )
                        {
                            (*tmp)->m_child[1]->m_child[1] = tmp1;
                            tmp1->m_child[0] = nullptr;
                        }
                        else{
                            tmp1->m_child[0] = (*tmp)->m_child[1]->m_child[1];
                            (*tmp)->m_child[1]->m_child[1] = tmp1;
                        }


                        tmp1 = (*tmp);
                        tmp = &(*tmp)->m_child[1];
                        cout<<"Nodo: "<<(*tmp)->m_data<<endl;
                        if(pila.empty()){
                            m_root = (*tmp);
                        }
                        if ((*tmp)->m_child[0])
                        {
                            tmp1->m_child[1] = (*tmp)->m_child[0];
                            (*tmp)->m_child[0] = tmp1;
                        }
                        else{
                            (*tmp)->m_child[0] = tmp1;
                            tmp1->m_child[1] = nullptr;
                        }
                        cout<<"aaa: "<<m_root->m_data<<endl;

                    }
                    
                    else if ((*tmp)->m_child[1]->m_balance == 1){
                        
                        tmp1 = (*tmp);
                        tmp = &(*tmp)->m_child[1];
                        cout<<"Nodo 1: "<<(tmp1)->m_child[1]->m_child[0]->m_data<<endl;
                        cout<<"Nodo 2: "<<(tmp1)->m_child[0]->m_child[1]->m_data<<endl;
                        if(pila.empty()){
                            m_root = (*tmp);
                        }
                        if ( (*tmp)->m_child[0] )
                        {
                            tmp2 = (*tmp);
                            tmp1->m_child[1] = tmp2->m_child[0];
                            tmp2->m_child[0] = tmp1;
                        }
                        else{
                            (*tmp)->m_child[0] = tmp1;
                            tmp1->m_child[1] = nullptr;
                        }
                    }
                    else if((*tmp)->m_child[1]->m_balance == 0){
                    
                    }
                }
                else if ((*tmp)->m_balance < -1){
                        
                    if ((*tmp)->m_child[0]->m_balance  == 1){

                        tmp1 = (*tmp)->m_child[0];
                        (*tmp)->m_child[0] = tmp1->m_child[1];
                        if ( !((*tmp)->m_child[0]->m_child[0]) )
                        {
                            (*tmp)->m_child[0]->m_child[0] = tmp1;
                            tmp1->m_child[1] = nullptr;
                        }
                        else{
                            tmp1->m_child[1] = (*tmp)->m_child[0]->m_child[0];
                            (*tmp)->m_child[0]->m_child[0] = tmp1;
                        }


                        tmp1 = (*tmp);
                        tmp = &(*tmp)->m_child[0];
                        tmp2 = (*tmp);
                        cout<<"Nodo 1: "<<(tmp1)->m_child[1]->m_child[0]->m_data<<endl;
                        cout<<"Nodo 2: "<<(tmp1)->m_child[0]->m_child[1]->m_data<<endl;
                        if(pila.empty()){
                            m_root = (*tmp);
                        }
                        if ( (*tmp)->m_child[1] )
                        {
                            tmp1->m_child[0] = tmp2->m_child[1];
                            tmp2->m_child[1] = tmp1;
                        }
                        else{
                            tmp1->m_child[0] = nullptr;
                            tmp2->m_child[1] = tmp1;
                        }
                        inorder();
                        cout<<"aaa: "<<m_root->m_child[1]->m_data<<endl;
                    }
                    else if ((*tmp)->m_child[0]->m_balance == -1){
                        tmp1 = (*tmp);
                        tmp = &(*tmp)->m_child[0];

                        if(pila.empty()){
                            m_root = (*tmp);
                        }
                        if ( (*tmp)->m_child[1] )
                        {
                            tmp2 = (*tmp);
                            tmp1->m_child[0] = tmp2->m_child[1];
                            tmp2->m_child[1] = tmp1;
                        }
                        else{
                            (*tmp)->m_child[1] = tmp1;
                            tmp1->m_child[0] = nullptr;
                        }
                    }
                    else if ((*tmp)->m_child[0]->m_balance == 0){

                    }
                    else{

                    }
                }
                balance_factor();



void balance_factor(){

            pair< CNode<T>*, int > tmp1;

            stack < pair< CNode<T>*, int> > pila = m_pila;
            
            tmp1.first = m_root;
            tmp1.second = 0;
            pila.push(tmp1);

            pair< CNode<T>*, int >* tmp(&pila.top());
            
            while(!pila.empty())
            {
                
                if(tmp->second == 0)
                {
                    tmp->second++;
                    if (tmp->first->m_child[1])
                    {
                        tmp1.first = tmp->first->m_child[1];
                        tmp1.second = 0;
                        pila.push(tmp1);
                    }
                }
                
                else if (tmp->second == 1)
                {
                    tmp->second++;
                    if (tmp->first->m_child[0])
                    {
                        tmp1.first = tmp->first->m_child[0];
                        tmp1.second = 0;
                        pila.push(tmp1);
                    }
                }

                else if (tmp->second == 2)
                {
                    if ( !tmp->first->m_child[0] && !tmp->first->m_child[1]){
                        tmp->first->m_balance = 0;
                        tmp->first->m_height = 1;
                    }
                    else{
                        if (!tmp->first->m_child[1]){
                            tmp->first->m_balance = 0 - tmp->first->m_child[0]->m_height;
                            tmp->first->m_height = tmp->first->m_child[0]->m_height + 1;

                        }
                        else if (!tmp->first->m_child[0]){
                            tmp->first->m_balance = tmp->first->m_child[1]->m_height;
                            tmp->first->m_height = tmp->first->m_child[1]->m_height + 1;
                        }
                        else{
                            
                            tmp->first->m_balance = tmp->first->m_child[1]->m_height - tmp->first->m_child[0]->m_height;
                            
                            if(tmp->first->m_child[1]->m_height > tmp->first->m_child[0]->m_height){
                                tmp->first->m_height = tmp->first->m_child[1]->m_height + 1;
                            }
                            else{
                                tmp->first->m_height = tmp->first->m_child[0]->m_height + 1;
                            }
                        }
                        
                    }
                    pila.pop();
            }
            tmp = &pila.top();
        }
                */