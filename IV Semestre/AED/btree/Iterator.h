#ifndef ITERATOR_H
#define ITERATOR_H

/**
 *	0->Visita izquierda
 *	1->Visita Nodo
 *	2->Visita Derecha
 *	3->Sube->(opcional)
**/
	
#include <tuple>
#include <stack>

template <typename T>
class CIterator{
	
	public:
		stack< pair<CNode<T>*, int> > m_pila;

		CIterator(stack< pair<CNode<T>*, int> > Pila = 0){
			m_pila = Pila;
		}

		void operator =(CIterator<T> x){
			m_pila = x.m_pila;
		}
		
		bool operator !=(CIterator<T> x){
			return m_pila != x.m_pila;
		}

		bool operator ==(CIterator<T> x){
			return m_pila == x.m_pila;
		}

		T operator *(){
			return m_pila.top().first->m_data;
		}

		CIterator<T> operator ++(int) = 0;
		
};


#endif // ITERATOR_H