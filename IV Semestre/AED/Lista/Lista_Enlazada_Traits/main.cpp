#include <iostream>
#include "Comparator.h"
#include "Lista.h"

using namespace std;


struct Traits{
    typedef int T;
    typedef Comp_Greater<T> H;
};

int main(){

    Lista<Traits> Lista1;

    for (int i = 10; i < 20; i++){
        Lista1.Insert(i);
    }

    Lista1.Print();

    cout<<endl;

}

