#include <iostream>
#include "Lista.h"

using namespace std;



template <typename T>
struct Comparator{
    virtual bool cmp(T, T);
};

template <typename T>
struct Comp_Less: Comparator<T>{
    inline bool cmp(T a, T b){
        return a < b;
    }
};

template <typename T>
struct Comp_Greater: Comparator<T>{
    inline bool cmp(T a, T b){
        return a > b;
    }
};


int main(){

    Lista<int, Comp_Less<int> > Lista1;

    for (int i = 10; i < 20; i++){
        Lista1.Insert(i);
    }

    Lista1.Print();

    cout<<endl;

}

