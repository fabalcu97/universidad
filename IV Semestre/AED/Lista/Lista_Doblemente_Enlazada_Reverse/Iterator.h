#ifndef ITERATOR_H
#define ITERATOR_H

#include "Node.h"

template<typename T>
class C_Iterator{

    public:
        Node<T>* m_it;

        C_Iterator(Node<T>* p = 0){
            m_it = p;
        }

        C_Iterator operator= (C_Iterator<T> x){
            m_it = x.m_it;
            return *this;
        }

        bool operator!= (C_Iterator<T> x){
            return m_it != x.m_it;
        }

        C_Iterator operator++(int){
            m_it = m_it->Next;
            return *this;
        }

        C_Iterator operator++(){
            m_it = m_it->Next;
            return *this;
        }

        T operator* (){
            return m_it->valor;
        }

};

#endif // ITERATOR_H
