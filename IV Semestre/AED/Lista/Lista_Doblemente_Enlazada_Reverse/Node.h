#ifndef NODE_H
#define NODE_H

template <typename T>
class Node{
    public:

        Node<T>* Next;
        T valor;

        Node(T x){
            valor = x;
            Next = nullptr;
        }

};

#endif // NODE_H
