#include <iostream>
#include "Lista.h"

using namespace std;

template <typename T>
struct ptr_func
{
    static bool (*ptr)(T, T);
};

template <typename T>
bool fless(T a, T b){

    return a < b;
}

template <typename T>
bool fgreater(T a, T b){
    return a > b;
}

//template <typename T> bool (*ptr_func<T>::ptr)(T, T) = fgreater;
template <typename T>
bool (*ptr_func<T>::ptr)(T, T) = fless;

int main(){


    Lista<int, ptr_func<int> > Lista1;

    for (int i = 10; i < 20; i++){
        Lista1.Insert(i);
    }

    Lista1.Print();

    cout<<endl;

}



