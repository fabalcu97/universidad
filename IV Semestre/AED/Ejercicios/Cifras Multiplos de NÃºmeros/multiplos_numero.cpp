#include<iostream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;

int main(){
	
	vector<int> multiplos;
	int n, i, tmp;

	cout<<"Ingrese el elemento: ";
	cin>>n;

	while(n > 1){
		for(i = 9; i >= 2; i--){
			if( n%i == 0 ){
				multiplos.push_back(i);
				n /= i;
				break;
			}
		}
	}
	if(n > 1){
		multiplos.push_back(n);
	}
	
	sort(multiplos.begin(), multiplos.end());
	for(int i = 0; i < multiplos.size(); i++){
		cout<<multiplos[i];
	}
	cout<<endl;

}

