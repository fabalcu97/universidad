﻿CREATE FUNCTION cargar()
RETURNS void AS
$BODY$BEGIN
for i in 1..100000 loop
	INSERT INTO usuario (nombre,dni,correo_electronico,usuario,contraseña,sexo, edad, fecha_modificacion_registro) VALUES
	( CONCAT('Preston Richards', i),1		+ i, CONCAT('justo@orciPhasellus.edu', i), CONCAT('Mccray', i),1 + i,'m',floor(random() * 80 + 10),'now'),
	( CONCAT('Ian Beck' , i),      650152   + i, CONCAT('Duis@duinec.co.uk', i), CONCAT('Hobbs', i),2 + i,'f',floor(random() * 80 + 10),'now'),
	( CONCAT('Cadman Walsh', i),   22401154 + i, CONCAT('Morbi@liberolacus.net', i),CONCAT('Cooley', i), 3 + i,'m',floor(random() * 80 + 10),'now'),
	( CONCAT('Flynn Young', i),    17836497 + i, CONCAT('urna.convallis.erat@primis.com', i),CONCAT('Gentry', i),4 + i,'f',floor(random() * 80 + 10),'now'),
	( CONCAT('Boris Alexander', i),11370773 + i, CONCAT('posuere.cubilia.Curae@Suspendisse.org', i), CONCAT('Pollard', i),5 + i,'m',floor(random() * 80 + 10),'now'),
	( CONCAT('Cruz Bartlett', i),  24042413 + i, CONCAT('Mauris@nibhPhasellus.com', i),CONCAT('Cotton', i),6 + i,'f',floor(random() * 80 + 10),'now'),
	( CONCAT('Edward Michael', i), 5786912  + i, CONCAT('eu.nulla@auctornon.edu', i), CONCAT('Nicholson', i),7 + i,'m',floor(random() * 80 + 10),'now'), 
	( CONCAT('Lewis Miles', i),    5998370  + i, CONCAT('et.risus@sapienmolestieorci.org', i),CONCAT('Crosby', i),8 + i,'f',floor(random() * 80 + 10),'now'),
	( CONCAT('Ian Lambert', i),    3439987  + i, CONCAT('auctor@mitempor.org', i),CONCAT('Pickett', i),9 + i,'m',floor(random() * 80 + 10),'now'),
	( CONCAT('Kenneth Battle', i), 18769466 + i, CONCAT('Nunc.commodo.auctor@urnanecluctus.co.uk', i),CONCAT('Booth', i),10+ i,'f',floor(random() * 80 + 10),'now');
end loop;
END$BODY$
LANGUAGE "plpgsql" VOLATILE;

select cargar();

DROP FUNCTION cargar()
