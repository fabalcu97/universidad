
CREATE INDEX ind_arbol ON usuario USING btree(dni);
CREATE INDEX ind_arbol ON usuario USING btree(nombre);
CLUSTER usuario USING ind_arbol;

CREATE INDEX ind_arbol ON usuario USING hash(dni);
CREATE INDEX ind_arbol ON usuario USING hash(nombre);

DROP INDEX ind_arbol;



DROP FUNCTION probar();

CREATE FUNCTION probar()
RETURNS void AS
$BODY$
DECLARE
  StartTime timestamptz;
  EndTime timestamptz;
  Delta interval;
  promedio numeric;

BEGIN
	promedio := 0;
	for i in 1..100 loop
		StartTime := clock_timestamp();
		PERFORM /*Query!!!!!------>*/
		EndTime := clock_timestamp();
		Delta := 1000 * ( extract(epoch from EndTime) - extract(epoch from StartTime) );
		promedio:=  (EXTRACT (EPOCH FROM  Delta )::float/3600 );
		RAISE NOTICE '%', promedio;
	end loop;
	
END
$BODY$
LANGUAGE "plpgsql" VOLATILE;

select probar();