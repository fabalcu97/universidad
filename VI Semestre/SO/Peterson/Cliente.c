#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXSIZE     27

//Servidor  ->  Proceso 1
//Cliente   ->  Proceso 2

int main()
{
    int ltr, flg, proc, turn;
    key_t key1, key2, key3, key4;
    int *letra;
    bool *flag;
	bool *procesos;
	int *turno;
    int otro_proc = 1;

    key1 = 5678;
    key2 = 5679;
	key3 = 5680;
	key4 = 5681;

    ltr = shmget(key1, MAXSIZE, 0666);
    flg = shmget(key2, 1, 0666);
	proc = shmget(key3, 2, 0666);
	turn = shmget(key4, sizeof(int), 0666);

    letra = shmat(ltr, NULL, 0);
    flag = shmat(flg, NULL, 0);
	procesos = shmat(proc, NULL, 0);
	turno = shmat(turn, NULL, 0);

    while(1){

        procesos[2] = true;
        *turno = 2;
        while( *turno == 2 && procesos[otro_proc] ){
            printf("Proc: %d  en región crítica.\n", otro_proc);
        }

        if( !(*flag) ){
            printf("Leo: %d\n", *letra);
            *flag = 1;
            sleep(2);
        }
    }

    exit(0);
}
