#include <iostream>
//#include "RRobin.cpp"
using namespace std;
#include "list.cpp"


void blink(void *args);

int main()
{
	Task tarea1(blink, 0, "blk1");
	Task tarea2(blink, 1, "blk2");
	Task tarea3(blink, 2, "blk3");
	Task tarea4(blink, 3, "blk4");
	Task tarea5(blink, 4, "blk5");

	List lista;
	lista.Insert(&tarea1);
	lista.Insert(&tarea2);
	lista.Insert(&tarea3);
	lista.Insert(&tarea4);
	lista.Insert(&tarea5);

	cout<<lista.t_head->t_name<<endl;

	lista.Watch();
	int t;
	cin>>t;

	return 0;

}

void blink(void *args)
{
	cout<<"Hola\n"<<endl;
}
