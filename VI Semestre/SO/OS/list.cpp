#include "list.h"

List::List()
{
	t_head = nullptr;
	t_tail = nullptr;
	t_size = 0;
}

void List::Insert(Task* nTask){

	if ( t_head == nullptr )
	{
		t_head = t_tail = nTask;
		nTask->def_next(nTask);
	}
	else
	{
		Task* tmP = t_tail;
		tmP->t_next = t_tail = nTask;
		t_tail->def_next(t_head);
	}
	t_size ++;
	return;
}

void List::Watch()
{
	Task** tmP = &t_head;
	do {
		cout<<(*tmP)->t_name<<"-\n";
		tmP = &(*tmP)->t_next;
	} while( *tmP != t_tail);
	cout<<(*tmP)->t_name<<"-\n";

}

int List::Size()
{
	return t_size;
}
