#include <iostream>
#include <vector>
#include "cpu.h"

using namespace std;

cpu::cpu(key_t key1, key_t key2){
	int flg = shmget(key1, FLAGSIZE, IPC_CREAT | 0666);
	m_flag = (bool*)shmat(flg, NULL, 0);
	*m_flag = 0;
	cout<<"holi"<<endl;
	m_cpuMEM = key2;

}

bool cpu::get_state(){
	return *m_flag;
}

void cpu::operacion(){

	int memsh = shmget(m_cpuMEM, INTSIZE, IPC_CREAT | 0666);
	key_t *data = (int*)shmat(memsh,NULL,0);
	int* A;
	memsh = shmget(*data, MATSIZE, IPC_CREAT | 0666);
	A = (int*)shmat(memsh,NULL,0);

	int opt = 0;

	while (1) {
		opt = A[0];
		if( opt == 4 ){
			cout<<"OPT: "<<opt<<endl;
			*m_flag = 1;
			cout<<"CPU ocupado"<<endl;
			sle( A[1] );
			*m_flag = 0;
			cout<<"CPU desocupado"<<endl;
			A[0] = 0;
		}
		if( opt == 3 ){
			cout<<"OPT: "<<opt<<endl;
			*m_flag = 1;
			cout<<"CPU ocupado"<<endl;
			sum( A[1], A[2] );
			*m_flag = 0;
			cout<<"CPU desocupado"<<endl;
			A[0] = 0;
		}
		if( opt == 2 ){
			cout<<"OPT: "<<opt<<endl;
			*m_flag = 1;
			cout<<"CPU ocupado"<<endl;
			rest( A[1], A[2] );
			*m_flag = 0;
			cout<<"CPU desocupado"<<endl;
			A[0] = 0;
		}
		if( opt == 1 ){
			cout<<"OPT: "<<opt<<endl;
			*m_flag = 1;
			cout<<"CPU ocupado"<<endl;
			fib( A[1] );
			*m_flag = 0;
			cout<<"CPU desocupado"<<endl;
			A[0] = 0;
		}
	}

}

void cpu::sum(int A, int B){
	cout<<A<<" + "<<B<<" = "<<A + B<<endl;
}

void cpu::rest(int A, int B){
	cout<<A<<" - "<<B<<" = "<<A - B<<endl;
}

void cpu::fib(int A){
	int a = 0,
		b = 1,
		c = 0;
	for (int i = 0; i < A; ++i) {
		c = a + b;
		a = b;
		b = c;
		cout<<c<<"-";
	}
	cout<<endl;
}

void cpu::sle(int A){
	sleep(A);
}


int main() {
	cpu procesador1(cpu1, cpu1MEM);
	cpu procesador2(cpu2, cpu2MEM);
	procesador1.operacion();
}

/*void cpu::printMatrix(key_t Key){

	int vect = shmget(Key, MATSIZE, 0666);

	vector<int> A = shmat(vect, NULL, 0);

	int 	AR_size = A.size(),
			AC_size = A[0].size();
	for (int i = 0; i < AR_size; ++i) {
		for (int j = 0; j < AC_size; ++j) {
			cout<<"|"<<A[i][j]<<"|";
		}
		cout<<"\n";
	}
}

vector< vector<int> > cpu::multMatrix(vector< vector<int> > A, vector< vector<int> > B){
	vector<vector<int> > result;

	if( A[0].size() != B.size() ){
		result.push_back(vector<int>());
		result[0].push_back(-1);
		return result;
	}

	int tmp = 0,
		AR_size = A.size(),
		AC_size = A[0].size(),
		BR_size = B.size(),
		BC_size = B[0].size();

	for (int i = 0; i < AR_size; i++) {
		result.push_back(vector<int>());
	}

	for (int i = 0; i < AR_size; ++i) {
		for (int j = 0; j < BC_size; ++j) {
			tmp = 0;
			for (int k = 0; k < AC_size; ++k) {
				//cout<<"i: "<<i<<"  j: "<<j<<"  k: "<<k<<endl;
				tmp += A[i][k] * B[k][j];
			}
			result[i].push_back(tmp);
		}
	}
	return result;
}*/

/*vector< vector<int> > cpu::TransMatrix(vector< vector<int> > A, vector< vector<int> > B){

}

vector<int> cpu::OrdElementos(vector<int> A){

}

vector< vector<int> > cpu::Invertir(vector< vector<int> > A){

}*/
