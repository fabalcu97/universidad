#define FLAGSIZE 1
#define LIBRE 0
#define OCUPADO 1
#define MATSIZE sizeof(int)*3
#define INTSIZE sizeof(int)

#define cpu1 5690
#define cpu2 5691

#define cpu1MEM 5692
#define cpu2MEM 5693
#define fibonacci 1
#define resta 2
#define suma 3
#define slp 4

#define memoria1 5670
#define memoria2 5671
#define memoria3 5672
#define memoria4 5673
#define memoria5 5674
#define memoria6 5675
#define memoria7 5676
#define memoria8 5677
#define memoria9 5678
#define memoria10 5679

#define pflag1 5680
#define pflag2 5681
#define pflag3 5682
#define pflag4 5683
#define pflag5 5684
#define pflag6 5685
#define pflag7 5686
#define pflag8 5687
#define pflag9 5688
#define pflag10 5689

/* si pflag es:
	1 ->	espera
	2 ->	ejecutando
	0 ->	finalizado
*/
