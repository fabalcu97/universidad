#include "scheduler.h"

scheduler::scheduler(){

	for (int i = 0; i < 10; ++i) {
		key_t key = 5670 + i;
		memoria tmp(key);
		m_Memorias.push_back(tmp);
	}
	for (int i = 0; i < 10; ++i) {
		int flg = shmget(5680 + i, FLAGSIZE, IPC_CREAT | 0666);
		int* flag = (int*)shmat(flg, NULL, 0);
		m_fproc.push_back(flag);
	}
	int tmp = shmget(cpu1, FLAGSIZE, 0666);
	bool* flg = (bool*)shmat(tmp, NULL, 0);
	m_CPU.push_back(flg);

	tmp = shmget(cpu2, FLAGSIZE, 0666);
	flg = (bool*)shmat(tmp, NULL, 0);
	m_CPU.push_back(flg);

	/*cout<<"CPU1: "<<*m_CPU[0]<<"... CPU2: "<<*m_CPU[1]<<endl;
	sleep(10);*/

}

void scheduler::exec(){

	int i = 0;
	while (1){
		if ( *m_fproc[i] == 1 ){		//estado del proceso
			//cout<<"Proceso [ "<<i<<" ] en ejecución..."<<endl;
			if ( !(m_Memorias[i].get_state()) ) {		//Si Memoria libre
				cout<<"flag("<<i<<"): "<<*m_fproc[i]<<endl;

				m_Memorias[i].set_flag(OCUPADO);		//Entonces Memoria ocupada
				*m_fproc[i] = 2;						//Proceso en ejecución
				cout<<"Cargando...\n";
				cout<<"Ejecutando...\n";
				cout<<"CPU1: "<<*m_CPU[0]<<"... CPU2: "<<*m_CPU[1]<<endl;

				if ( !(*m_CPU[0]) ) {
					//cout<<"Envio CPU1...\n";
					int m_cpu1 = shmget(cpu1MEM, INTSIZE, 0666);
					key_t* tmp = (int*)shmat(m_cpu1, NULL, 0);
					*tmp = m_Memorias[i].get_key();
				}
				else if ( !(*m_CPU[1]) ) {
					//cout<<"Envio CPU2...\n";
					int m_cpu2 = shmget(cpu2MEM, INTSIZE, 0666);
					key_t* tmp = (int*)shmat(m_cpu2, NULL, 0);
					*tmp = m_Memorias[i].get_key();
				}
				cout<<"CPU1: "<<*m_CPU[0]<<"... CPU2: "<<*m_CPU[1]<<endl;
			}
		}
		if ( *m_fproc[i] == 0 ){
			m_Memorias[i].set_flag(LIBRE);
		}
		++i;
		if (i == 10) {
			i = 0;
		}
	}

}

int main(){
	scheduler sched;
	sched.exec();
}
