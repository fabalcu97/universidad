#include <iostream>
#include <vector>
#include "variables.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

using namespace std;

int main(){

	int flg = shmget(pflag1, FLAGSIZE, 0666);
	int* flag = (int*)shmat(flg, NULL, 0);
	*flag = 1;

	cout<<"Esperando..."<<endl;
	while ( *flag == 1 );

	if ( *flag == 2 ) {

		int memsh = shmget(memoria1, MATSIZE, IPC_CREAT | 0666);

		int* A = (int*)shmat(memsh,NULL,0);

		A[0] = suma;
		A[1] = 2;
		A[2] = 5;

		cout<<"Ejecución..."<<endl;
		while ( A[0] != 0 );
		shmctl(memoria1, IPC_RMID, (struct shmid_ds *)NULL);
		*flag = 0;
	}
	cout<<"¡Proceso finalizado!"<<endl;
	/*char opt;
	cin>>opt;*/
	return 0;

}

/*int op = shmget(operacion1, INTSIZE, IPC_CREAT | 0666);
int* operacion = (int*)shmat(op, NULL, 0);
*operacion = print;

int memsh = shmget(memoria1, MATSIZE, IPC_CREAT | 0666);

void* rp = (void*)shmat(memsh,NULL,0);
vector<vector<int> >* A = new(rp) vector<vector<int> >;

(*A) = {
	{1, 2, 3},
	{4, 5, 6},
	{7, 8, 9}
};*/
