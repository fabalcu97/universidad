#include "memoria.h"

memoria::memoria(key_t key){
	m_key = key;
	m_flag = 0;
}

void memoria::set_flag(bool flag){
	m_flag = flag;
}

bool memoria::get_state(){
	return m_flag;
}

key_t memoria::get_key(){
	return m_key;
}
