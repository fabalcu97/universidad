#include <iostream>
#include <vector>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define FLAG 1
#define MATSIZE 100
#define INT sizeof(int)

using namespace std;

int main(){

	int flg = shmget(5680, FLAG, IPC_CREAT | 0666);
	bool flag = shmat(flg, NULL, 0);

	flag = 1;

	int memsh = shmget(5670, MATSIZE, IPC_CREAT | 0666);

	void* rp = (void*)shmat(memsh,NULL,0);
	vector<vector<int> >* A = new(rp) vector<vector<int> >;

	(*A) = {
				{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9}
			};

	flag = 0;

}
