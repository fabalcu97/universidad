#include <iostream>

#include "cpu.h"

using namespace std;

int main(){

	cpu procesador1(cpu1, cpu1MEM);
	cpu procesador2(cpu2, cpu2MEM);
	procesador1.operacion();

}
