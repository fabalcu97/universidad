#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

using namespace std;

class memoria{

	private:

	key_t m_key;
	bool m_flag;

	public:
		memoria(key_t);
		void set_flag(bool);
		bool get_state();
		key_t get_key();

};
