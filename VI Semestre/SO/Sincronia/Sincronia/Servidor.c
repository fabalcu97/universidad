#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXSIZE     27

int main()
{
    char c;
    int shmid, flag;
    key_t key1, key2;
    int *shm;
    bool *flg;

    key1 = 5678;
    key2 = 5679;

    shmid = shmget(key1, MAXSIZE, IPC_CREAT | 0666);
    flag = shmget(key2, MAXSIZE, IPC_CREAT | 0666);

    shm = shmat(shmid, NULL, 0);
    flg = shmat(flag, NULL, 0);

    *flg = 1;

    while(1){
        if(*flg){
            *shm = rand() % 10;
            *flg = 0;
            printf("Escribo: %d\n", *shm);
            sleep(2);
        }
    }

    exit(0);
}
