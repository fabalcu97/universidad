#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

bool *flag;
bool *turno;

void entrar(bool proceso)
{
    int otro = 1-proceso;
    flag[proceso] = true;
    *turno = proceso;
    while( *turno == proceso && flag[otro] == true );
}

void salir(bool proceso)
{
    flag[proceso]=false;
}

int main (int argc, char *argv[]) {

    int serv, procs, id;
    const char* tmp;
    int idf, idt, ids, idfs;

    tmp = argv[1];
    serv = atoi(tmp);
    tmp = argv[2];
    procs = atoi(tmp);
    tmp = argv[3];
    id = atoi(tmp);

    printf("Serv: %d\n", serv);
    printf("Procs: %d\n", procs);
    printf("ID: %d\n", id);
    exit(0);

    int* data_serv;
    bool* flag_serv;

    key_t   keyf,       //flag's
            keyt,       //turnos
            keys,       //servidor compartido
            keyfs;      //servidor booleanos

    keyt = 1235;
    keyf = 1236;
    keys = 1237;
    keyfs = 1238;

    idt = shmget(keyt, procs*sizeof(bool), 0666);
    idf = shmget(keyf, procs*sizeof(bool), 0666);
    ids = shmget(keys, serv*sizeof(int), 0666);
    idfs = shmget(keyfs, serv*sizeof(bool), 0666);

    turno = (bool*)shmat(idt, NULL, 0);
    flag = (bool*)shmat(idf, NULL, 0);
    data_serv = (int*)shmat(ids, NULL, 0);
    flag_serv = (bool*)shmat(idfs, NULL, 0);

    flag_serv[id] = true;

    while(1){
        if(flag_serv[id]){
            data_serv[id] = rand() % 10;
            flag_serv[id] = false;
            printf("Escribo: %d\n", data_serv[id]);
        }
    }

    exit(0);
 }
