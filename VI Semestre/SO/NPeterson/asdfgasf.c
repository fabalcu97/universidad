#include  <stdio.h>
#include  <sys/types.h>

#define   MAX_COUNT  200

void  ChildProcess(void);                /* child process prototype  */
void  ParentProcess(void);               /* parent process prototype */

void  main(void)
{
     pid_t  pid;

     pid = fork();
     if (pid == 0)
          ChildProcess();
     else
          ParentProcess();
}

void  ChildProcess(void)
{
     int   i;

     for (i = 1; i <= MAX_COUNT; i++)
          printf("   This line is from child, value = %d\n", i);
     printf("   *** Child process is done ***\n");
}

void  ParentProcess(void)
{
     int   i;

     for (i = 1; i <= MAX_COUNT; i++)
          printf("This line is from parent, value = %d\n", i);
     printf("*** Parent is done ***\n");
}



/*#include  <stdio.h>
#include  <sys/types.h>

void parse(char*, char**);

void main(void){
    char* argv[64];

    char line[] = "./Servidor 1 2 3";

    parse(line, argv);
    execvp(*argv, argv);
}


void  parse(char *line, char **argv)
{
     while (*line != '\0') {
        while (*line == ' ' || *line == '\t' || *line == '\n'){
             *line++ = '\0';
        }
        *argv++ = line;
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n'){
            line++;
        }
     }
     *argv = '\0';
}
*/
